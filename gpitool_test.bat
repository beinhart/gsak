cd %0%\..

set prog=x64\release\gpitool.exe
set prog=x64\debug\gpitool.exe
set logfile=output.txt
set inputfolder=%userprofile%\Documents\temp\pois-gpi

del %logfile%
for /d %%i in (%inputfolder%\*) do for %%j in ("%%i\*.gpi") do call :process_file "%%j" >> %logfile%
exit /b 0

:process_file
del gpitool.output.gpi
%prog% %*
fc /b %* gpitool.output.gpi
exit /b 0
