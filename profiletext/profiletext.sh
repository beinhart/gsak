#!/bin/bash

set -eu

INPUT1=profile.txt
INPUT2=/mnt/c/Users/${USER}/AppData/Roaming/gsak/html/stats1.html 
OUTPUT=output.txt

cp "${INPUT1}" "${OUTPUT}"
awk '/<\/body>/{found=0} {if(found) print} /<body>/{found=1}' "${INPUT2}" >>"${OUTPUT}"
dos2unix "${OUTPUT}"
sed -i '/^$/d' "${OUTPUT}"
notepad.exe "${OUTPUT}"
