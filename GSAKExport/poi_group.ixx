module;

#include <compare>
#include <filesystem>
#include <ostream>
#include <string>
#include <vector>

export module poi_group;

import point_of_interest;
export struct poi_group_and_subgroup_names {
	poi_group_and_subgroup_names(std::wstring group_, std::wstring subgroup_) noexcept
		: group{ std::move(group_) }
		, subgroup{ std::move(subgroup_) }
	{
	}

	constexpr [[nodiscard]] bool has_subgroup() const noexcept { return !subgroup.empty(); }

	std::filesystem::path get_path_gpx(std::filesystem::path path) const
	{
		path /= group;
		if (has_subgroup()) {
			path /= subgroup;
		}
		path += L".gpx";
		return path;
	}

	constexpr auto operator<=>(poi_group_and_subgroup_names const&) const noexcept = default;

	friend std::wostream& operator<<(std::wostream& os, poi_group_and_subgroup_names val)
	{
		os << val.group;
		if (val.has_subgroup()) {
			os << L'/' << val.subgroup;
		}
		return os;
	}

	std::wstring group;		 // The name of the group.
	std::wstring subgroup;	 // Optional name of the subgroup (empty if no subgroup).
};

export struct poi_group {
	poi_group_and_subgroup_names names;	   // The names of group and subgroup.
	std::wstring prefix_name;			   // Optional prefix to put before the name of any poi in pois (empty if not set).
	std::vector<point_of_interest> pois;   // The points of interest.
};
