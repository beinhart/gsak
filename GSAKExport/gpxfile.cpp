#include "gpxfile.h"

#include <cassert>
#include <chrono>
#include <ctime>
#include <format>
#include <iostream>
#include <iterator>
#include <regex>
#include <sstream>
#include <string>
#include <string_view>

#include <comdef.h>

#include <gsl/pointers>

//#include <MsXml6.h>
//#include <OleAuto.h>

// https://docs.microsoft.com/en-us/previous-versions/windows/desktop/ms759192(v=vs.85)

namespace {
	constexpr wchar_t namespace_gpx[]{ L"http://www.topografix.com/GPX/1/0" };
	bstr_t const bstr_namespace_gpx{ namespace_gpx };
	constexpr wchar_t namespace_groundspeak[]{ L"http://www.groundspeak.com/cache/1/0/1" };
	bstr_t const bstr_namespace_groundspeak{ namespace_groundspeak };

	namespace config {
		constexpr bool filter_poi_names{ true };
	};

	std::wstring map_type_gsak_to_groundspeak(std::wstring const& str);

	void check_com_return_code(HRESULT hr);
	void add_attribute(IXMLDOMDocument& doc, IXMLDOMElement& node, std::wstring const& name, variant_t value);
	void add_simple_node(IXMLDOMDocument& doc, IXMLDOMElement& parent, std::wstring const& tag_name, bstr_t const& ns, variant_t value);
	IXMLDOMElementPtr create_node(IXMLDOMDocument& doc, std::wstring const& tag_name, bstr_t const& ns);
	IXMLDOMElementPtr create_groundspeak_node_with_attribute(IXMLDOMDocument& doc, std::wstring const& tag_name,
															 std::wstring const& attribute_name, bool attribute_value, std::wstring const& value);

	std::wstring filter_poi_name(std::wstring name);

	geocache_log make_first_log(geocache const& cache);
}	// namespace

com_initialize::com_initialize() [[gsl::suppress(f .6)]]
{
	check_com_return_code(CoInitialize(nullptr));
}

com_initialize::~com_initialize() noexcept
{
	CoUninitialize();
}

gpxfile::gpxfile() [[gsl::suppress(f .6)]]
{
	IXMLDOMDocument3Ptr doc;
	HRESULT hr{ doc.CreateInstance(__uuidof(DOMDocument60), nullptr, CLSCTX_INPROC_SERVER) };
	check_com_return_code(hr);
	hr = doc->put_async(VARIANT_FALSE);
	check_com_return_code(hr);
	hr = doc->put_resolveExternals(VARIANT_TRUE);
	check_com_return_code(hr);
	hr = doc->put_preserveWhiteSpace(VARIANT_TRUE);
	check_com_return_code(hr);

	// Everything was successful, exchange local variables and member variables.
	_doc.Attach(doc.Detach());
}

[[gsl::suppress(bounds .3)]] IXMLDOMElementPtr gpxfile::prepare(bool with_ns_groundspeak) const
{
	using namespace std::string_literals;

	IXMLDOMProcessingInstructionPtr pi;
	HRESULT hr = _doc->createProcessingInstruction(bstr_t{ L"xml" }, bstr_t{ L"version='1.0' encoding='utf-8'" }, &pi);
	check_com_return_code(hr);
	IXMLDOMNodePtr node;
	hr = _doc->appendChild(pi, &node);
	check_com_return_code(hr);
	IXMLDOMElementPtr root;
	hr = _doc->createElement(bstr_t{ L"gpx" }, &root);
	check_com_return_code(hr);
	//	hr = doc->setProperty(bstr_t{ L"SelectionNamespaces" }, variant_t{ L"xmlns='http://www.topografix.com/GPX/1/1' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'" });
	//	check_com_return_code(hr);
	add_attribute(_doc, root, L"xmlns:xsd", L"http://www.w3.org/2001/XMLSchema");
	add_attribute(_doc, root, L"xmlns:xsi", L"http://www.w3.org/2001/XMLSchema-instance");
	add_attribute(_doc, root, L"xmlns", namespace_gpx);
	std::wstring schema_location{ std::wstring{ namespace_gpx } + L' ' + namespace_gpx + L"/gpx.xsd" };
	if (with_ns_groundspeak) {
		add_attribute(_doc, root, L"xmlns:groundspeak", namespace_groundspeak);
		schema_location += L" "s + namespace_groundspeak + L' ' + namespace_groundspeak + L"/cache.xsd";
	}
	add_attribute(_doc, root, L"xsi:schemaLocation", schema_location.c_str());
	add_attribute(_doc, root, L"version", L"1.0");
	add_attribute(_doc, root, L"creator", L"GSAKExport");
	//TODO add bounds?
	hr = _doc->appendChild(root, nullptr);
	check_com_return_code(hr);

	return root;
}

void gpxfile::add(IXMLDOMElement& parent, std::vector<geocache> const& geocaches) const
{
	for (geocache const& cache : geocaches) {
		add(parent, cache);
	}
}

void gpxfile::add(IXMLDOMElement& parent, geocache const& cache) const
{
	using namespace std::string_literals;

	IXMLDOMElementPtr wpt{ create_node(_doc, L"wpt", bstr_namespace_gpx) };
	add_attribute(_doc, wpt, L"lat", cache.latitude.c_str());
	add_attribute(_doc, wpt, L"lon", cache.longitude.c_str());

	add_simple_node(_doc, wpt, L"name", bstr_namespace_gpx, cache.code.c_str());
	add_simple_node(_doc, wpt, L"type", bstr_namespace_gpx, (L"Geocache|"s + map_type_gsak_to_groundspeak(cache.type)).c_str());

	IXMLDOMElementPtr gs_cache{ create_node(_doc, L"groundspeak:cache", bstr_namespace_groundspeak) };
	add_attribute(_doc, gs_cache, L"id", cache.id.c_str());
	add_attribute(_doc, gs_cache, L"available", cache.available);
	add_attribute(_doc, gs_cache, L"archived", false);
	add_simple_node(_doc, gs_cache, L"groundspeak:name", bstr_namespace_groundspeak, cache.name.c_str());
	add_simple_node(_doc, gs_cache, L"groundspeak:placed_by", bstr_namespace_groundspeak, cache.placed_by.c_str());
	add_simple_node(_doc, gs_cache, L"groundspeak:type", bstr_namespace_groundspeak, variant_t{ map_type_gsak_to_groundspeak(cache.type).c_str() });
	add_simple_node(_doc, gs_cache, L"groundspeak:container", bstr_namespace_groundspeak, cache.container.c_str());
	add_simple_node(_doc, gs_cache, L"groundspeak:difficulty", bstr_namespace_groundspeak, cache.difficulty);
	add_simple_node(_doc, gs_cache, L"groundspeak:terrain", bstr_namespace_groundspeak, cache.terrain);

	if (!cache.short_desc.empty()) {
		const IXMLDOMElementPtr node{
			create_groundspeak_node_with_attribute(_doc, L"groundspeak:short_description",
												   L"html", cache.short_desc_is_html, cache.short_desc)
		};
		HRESULT const hr{ gs_cache->appendChild(node, nullptr) };
		check_com_return_code(hr);
	}
	if (!cache.long_desc.empty()) {
		const IXMLDOMElementPtr node{
			create_groundspeak_node_with_attribute(_doc, L"groundspeak:long_description",
												   L"html", cache.long_desc_is_html, cache.long_desc)
		};
		HRESULT const hr{ gs_cache->appendChild(node, nullptr) };
		check_com_return_code(hr);
	}
	if (!cache.hint.empty()) {
		add_simple_node(_doc, gs_cache, L"groundspeak:encoded_hints", bstr_namespace_groundspeak, cache.hint.c_str());
	}

	IXMLDOMElementPtr gs_logs{ create_node(_doc, L"groundspeak:logs", bstr_namespace_groundspeak) };
	add(gs_logs, make_first_log(cache));
	add(gs_logs, cache.last_logs);
	add(gs_logs, cache.my_logs);
	HRESULT hr{ gs_cache->appendChild(gs_logs, nullptr) };
	check_com_return_code(hr);

	hr = wpt->appendChild(gs_cache, nullptr);
	check_com_return_code(hr);
	hr = parent.appendChild(wpt, nullptr);
	check_com_return_code(hr);
}

void gpxfile::add(IXMLDOMElement& parent, std::vector<geocache_log> const& logs) const
{
	for (geocache_log const& log : logs) {
		add(parent, log);
	}
}

void gpxfile::add(IXMLDOMElement& parent, geocache_log const& log) const
{
	IXMLDOMElementPtr gs_log{ create_node(_doc, L"groundspeak:log", bstr_namespace_groundspeak) };
	if (!log.date_time.empty()) {
		add_simple_node(_doc, gs_log, L"groundspeak:date", bstr_namespace_groundspeak, log.date_time.c_str());
	}
	add_simple_node(_doc, gs_log, L"groundspeak:type", bstr_namespace_groundspeak, log.type.c_str());
	if (!log.by.empty()) {
		add_simple_node(_doc, gs_log, L"groundspeak:finder", bstr_namespace_groundspeak, log.by.c_str());
	}
	const IXMLDOMElementPtr node{
		create_groundspeak_node_with_attribute(_doc, L"groundspeak:text", L"encoded", log.encoded, log.text)
	};
	HRESULT hr = gs_log->appendChild(node, nullptr);
	check_com_return_code(hr);
	if (!log.lat.empty() && !log.lon.empty()) {
		IXMLDOMElementPtr gs_log_wpt{ create_node(_doc, L"groundspeak:log_wpt", bstr_namespace_groundspeak) };
		add_attribute(_doc, gs_log_wpt, L"lat", log.lat.c_str());
		add_attribute(_doc, gs_log_wpt, L"lon", log.lon.c_str());
		hr = gs_log->appendChild(gs_log_wpt, nullptr);
		check_com_return_code(hr);
	}

	hr = parent.appendChild(gs_log, nullptr);
	check_com_return_code(hr);
}

void gpxfile::add(IXMLDOMElement& parent, point_of_interest const& poi, std::wstring const& prefix_name) const
{
	IXMLDOMElementPtr wpt{ create_node(_doc, L"wpt", bstr_namespace_gpx) };
	add_attribute(_doc, wpt, L"lat", poi.latitude.c_str());
	add_attribute(_doc, wpt, L"lon", poi.longitude.c_str());
	std::wstring const name{ prefix_name + poi.name };
	add_simple_node(_doc, wpt, L"name", bstr_namespace_gpx, filter_poi_name(name).c_str());
	if (!poi.desc.empty()) {
		add_simple_node(_doc, wpt, L"desc", bstr_namespace_gpx, poi.desc.c_str());
	}
	HRESULT const hr{ parent.appendChild(wpt, nullptr) };
	check_com_return_code(hr);
}

void gpxfile::validate() const
{
	IXMLDOMParseErrorPtr error;
	HRESULT hr = _doc->validate(&error);
	check_com_return_code(hr);
	long error_code{};
	hr = error->get_errorCode(&error_code);
	check_com_return_code(hr);
	if (error_code) {
		bstr_t str;
		hr = error->get_reason(str.GetAddress());
		check_com_return_code(hr);
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ std::cerr }, "gpxfile is not valid. error_code {} reason {}\n", error_code, static_cast<char const*>(str));
	}
}

void gpxfile::save(std::filesystem::path const& filename) const
{
	variant_t v{ filename.c_str() };
	HRESULT const hr{ _doc->save(v) };
	check_com_return_code(hr);
}

namespace {
	std::wstring map_type_gsak_to_groundspeak(std::wstring const& str)
	{
		if (str == L"A") {	 // Project Ape
			return L"Traditional Cache";
		}
		if (str == L"B") {
			return L"Letterbox Hybrid";
		}
		if (str == L"C") {
			return L"Cache In Trash Out Event";
		}
		if (str == L"D") {	 // Groundspeak Lost and Found Celebration
			return L"Event Cache";
		}
		if (str == L"E") {
			return L"Event";
		}
		if (str == L"F") {	 // Community Celebration Event, formerly Lost and Found Event
			return L"Event Cache";
		}
		if (str == L"I") {
			return L"Wherigo Cache";
		}
		if (str == L"U") {
			return L"Unknown Cache";
		}
		if (str == L"H") {	 // Groundspeak HQ Cache
			return L"Unknown Cache";
		}
		if (str == L"J") {
			return L"Giga-Event Cache";
		}
		if (str == L"L") {
			return L"Locationless (Reverse) Cache";
		}
		if (str == L"M") {
			return L"Multi-cache";
		}
		if (str == L"O") {	 // Community Celebration Event
			return L"Event Cache";
		}
		if (str == L"P") {	 // Groundspeak Block Party
			return L"Event Cache";
		}
		if (str == L"R") {
			return L"Earthcache";
		}
		if (str == L"T") {
			return L"Traditional Cache";
		}
		if (str == L"U") {
			return L"Unknown Cache";
		}
		if (str == L"V") {
			return L"Virtual Cache";
		}
		if (str == L"W") {
			return L"Webcam Cache";
		}
		if (str == L"X") {
			return L"GPS Adventures Exhibit";
		}
		if (str == L"Z") {
			return L"Mega-Event Cache";
		}
		//G = BenchMark
		//Q = Lab cache
		//Y = Waymark -> exported as Waypoint, not as Geocache Waypoint

		assert(false);
		return L"";
	}

	void check_com_return_code(HRESULT hr)
	{
		if (FAILED(hr)) {
			_com_raise_error(hr, nullptr);
		}
	}

	void add_attribute(IXMLDOMDocument& doc, IXMLDOMElement& node, std::wstring const& name, variant_t value)
	{
		IXMLDOMAttributePtr attrib;
		HRESULT hr{ doc.createAttribute(bstr_t{ name.c_str() }, &attrib) };
		check_com_return_code(hr);

		if (value.vt == VT_BOOL) {
			hr = VariantChangeType(&value, &value, VARIANT_ALPHABOOL, VT_BSTR);
			check_com_return_code(hr);
		}

		hr = attrib->put_value(value);
		check_com_return_code(hr);

		IXMLDOMAttributePtr old_attrib;
		hr = node.setAttributeNode(attrib, &old_attrib);
		check_com_return_code(hr);
	}

	void add_simple_node(IXMLDOMDocument& doc, IXMLDOMElement& parent, std::wstring const& tag_name, bstr_t const& ns, variant_t value)
	{
		IXMLDOMElementPtr node{ create_node(doc, tag_name, ns) };

		HRESULT hr = VariantChangeTypeEx(&value, &value, MAKELCID(MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), SORT_DEFAULT), VARIANT_NOUSEROVERRIDE, VT_BSTR);
		check_com_return_code(hr);

		IXMLDOMTextPtr node_text;
		hr = doc.createTextNode(value.bstrVal, &node_text);
		check_com_return_code(hr);

		hr = node->appendChild(node_text, nullptr);
		check_com_return_code(hr);

		hr = parent.appendChild(node, nullptr);
		check_com_return_code(hr);
	}

	IXMLDOMElementPtr create_node(IXMLDOMDocument& doc, std::wstring const& tag_name, bstr_t const& ns)
	{
		IXMLDOMNodePtr node;
		HRESULT const hr{ doc.createNode(variant_t{ NODE_ELEMENT }, bstr_t{ tag_name.c_str() }, ns, &node) };
		check_com_return_code(hr);
		return node;
	}

	IXMLDOMElementPtr create_groundspeak_node_with_attribute(IXMLDOMDocument& doc, std::wstring const& tag_name,
															 std::wstring const& attribute_name, bool attribute_value, std::wstring const& value)
	{
		IXMLDOMElementPtr node{ create_node(doc, tag_name, bstr_namespace_groundspeak) };
		add_attribute(doc, node, attribute_name, attribute_value);

		IXMLDOMTextPtr node_text;
		HRESULT hr{ doc.createTextNode(bstr_t{ value.c_str() }, &node_text) };
		check_com_return_code(hr);

		hr = node->appendChild(node_text, nullptr);
		check_com_return_code(hr);

		return node;
	}

	geocache_log make_first_log(geocache const& cache)
	{
		geocache_log log{ .type{ L"Write Note" } };
		log.text = cache.first_log_prefix;
		for (geocache_attribute const& attribute : cache.attributes) {
			log.text += L"<br/>";
			log.text += (attribute.inc ? L"Yes: " : L"No: ");
			log.text += attribute.id_as_string();
		}
		if (!cache.first_log_gcnote.empty()) {
			log.text += L"<br/>GCNote: ";
			log.text += cache.first_log_gcnote;
		}
		return log;
	}

	std::wstring filter_poi_name(std::wstring name)
	{
		if constexpr (config::filter_poi_names) {
			for (;;) {
				std::wstring result{ std::regex_replace(name, std::wregex{ L"@(.*[[:digit:]])" }, L"(at)$1") };
				if (result == name) {
					return result;
				}
				name = std::move(result);
			}
		}
		else {
			return std::move(name);
		}
	}
}	// namespace
