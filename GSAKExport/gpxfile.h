#ifndef GPXFILE_H_INCLUDED
#define GPXFILE_H_INCLUDED

#include <filesystem>
#include <string>
#include <vector>

#include <comdef.h>	  // _COM_SMARTPTR_TYPEDEF

//#include <objbase.h>
#include <MsXml6.h>	  // IXMLDOMDocument3, IXMLDOMElementPtr

import geocache;
import point_of_interest;

class com_initialize {
public:
	com_initialize();
	com_initialize(com_initialize const&)	  = delete;
	com_initialize(com_initialize&&) noexcept = delete;
	com_initialize& operator=(com_initialize const&) = delete;
	com_initialize& operator=(com_initialize&&) noexcept = delete;
	~com_initialize() noexcept;
};

_COM_SMARTPTR_TYPEDEF(IXMLDOMDocument3, __uuidof(IXMLDOMDocument3));

class gpxfile {
public:
	gpxfile();
	gpxfile(gpxfile const&)		= delete;
	gpxfile(gpxfile&&) noexcept = delete;
	gpxfile& operator=(gpxfile const&) = delete;
	gpxfile& operator=(gpxfile&&) noexcept = delete;
	~gpxfile() noexcept					   = default;

	IXMLDOMElementPtr prepare(bool with_ns_groundspeak) const;

	void add(IXMLDOMElement& parent, std::vector<geocache> const& geocaches) const;
	void add(IXMLDOMElement& parent, geocache const& cache) const;
	void add(IXMLDOMElement& parent, std::vector<geocache_log> const& logs) const;
	void add(IXMLDOMElement& parent, geocache_log const& log) const;
	void add(IXMLDOMElement& parent, point_of_interest const& poi, std::wstring const& prefix_name) const;
	void validate() const;
	void save(std::filesystem::path const& filename) const;

private:
	IXMLDOMDocument3Ptr _doc;
};

#endif	 // GPXFILE_H_INCLUDED
