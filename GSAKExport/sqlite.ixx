module;

#include <filesystem>
#include <stdexcept>
#include <string>
#include <string_view>

#include <winsqlite/winsqlite3.h>

export module sqlite;

export class sqlite {
public:
	sqlite(std::filesystem::path const& filename);
	sqlite(sqlite const&)	  = delete;
	sqlite(sqlite&&) noexcept = default;
	sqlite& operator=(sqlite const&) = delete;
	sqlite& operator=(sqlite&&) noexcept = default;
	~sqlite() noexcept;

	int errcode() const noexcept { return sqlite3_errcode(_db); }
	int extended_errcode() const noexcept { return sqlite3_extended_errcode(_db); }

	operator sqlite3*() const noexcept { return _db; }

private:
	sqlite3* _db{};
};

export class sqlite_stmt {
public:
	sqlite_stmt(sqlite const& db, std::wstring const& sql);
	sqlite_stmt(sqlite_stmt const&) = delete;
	sqlite_stmt(sqlite_stmt&&)		= delete;
	sqlite_stmt& operator=(sqlite_stmt const&) = delete;
	sqlite_stmt& operator=(sqlite_stmt&&) = delete;
	~sqlite_stmt() noexcept;

	bool step() const;

	operator sqlite3_stmt*() const noexcept { return _stmt; }

private:
	sqlite3_stmt* _stmt{};
	sqlite const* const _db{};
};

export struct sqlite_exception : std::exception {
	sqlite_exception(sqlite const& db, int result, std::string_view function)
		: _str{ get_str(db, result, function) }
	{}

	virtual const char* what() const noexcept
	{
		return _str.c_str();
	}

	static std::string get_str(sqlite const& db, int result, std::string_view function);

	std::string const _str;
};

export std::wstring sqlite3_column_wstring(sqlite3_stmt* stmt, int iCol);

export void check_sqlite_return_code(sqlite const& db, int result, std::string_view function);
