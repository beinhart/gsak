module;

#include <filesystem>
#include <optional>
#include <string>

export module poi_export_config;

import poi_group;

export struct poi_export_config {
	std::optional<std::filesystem::path> path_gpx;
	std::optional<std::filesystem::path> path_gpi;
	std::optional<std::filesystem::path> path_images;
	std::wstring prefix_gc{ L"GC" };
	std::wstring prefix_gc_found{ L"GC_Found" };
	std::wstring prefix_gc_placed{ L"GC_Placed" };
	bool subfolder_gc{ false };
	bool subfolder_gc_found{ false };
	bool subfolder_gc_placed{ false };

#if 0
	std::filesystem::path make_gpx_folder(bool add_folder, std::wstring const& prefix) const {
		std::filesystem::path folder{ path_gpx.value() };
		if (add_folder) {
			folder /= prefix;
		}
		return folder;
	}

	static std::wstring make_filename(bool add_folder, std::wstring const& prefix, std::wstring filename) {
		if (!add_folder) {
			filename = prefix + L' ' + filename;
		}
		if (filename.empty()) {
			filename = prefix;
		}
		return filename + L".gpx";
	}
#endif

	poi_group_and_subgroup_names get_poi_groups_gc(std::wstring group) const
	{
		return get_poi_groups(subfolder_gc, prefix_gc, std::move(group));
	}

	poi_group_and_subgroup_names get_poi_groups_gc_found(std::wstring group) const
	{
		return get_poi_groups(subfolder_gc_found, prefix_gc_found, std::move(group));
	}

	poi_group_and_subgroup_names get_poi_groups_gc_placed(std::wstring group) const
	{
		return get_poi_groups(subfolder_gc_placed, prefix_gc_placed, std::move(group));
	}

private:
	static poi_group_and_subgroup_names get_poi_groups(bool subfolder, std::wstring const& prefix, std::wstring group)
	{
		if (group.empty()) {
			group = prefix;
		}
		if (subfolder) {
			return { prefix, std::move(group) };
		}
		else {
			return { prefix + L' ' + std::move(group), {} };
		}
	}
};
