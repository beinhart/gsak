module;

#include <string>

module geocache;

std::wstring geocache_attribute::id_as_string() const
{
	// grep "groundspeak:attribute id" /mnt/f/Garmin/1789757.gpx | sed -e 's/<groundspeak:attribute id="/case /' -e 's/" inc="[01]">/: return L"/' -e 's/<\/groundspeak:attribute>/";/' | sort -k 2 -g | uniq
	switch (id) {
	case 1: return L"Dogs";
	case 2: return L"Access/parking fee";
	case 3: return L"Climbing gear required";
	case 4: return L"Boat required";
	case 5: return L"Scuba gear required";
	case 6: return L"Recommended for kids";
	case 7: return L"Takes less than one hour";
	case 8: return L"Scenic view";
	case 9: return L"Significant hike";
	case 10: return L"Difficult climb";
	case 11: return L"May require wading";
	case 12: return L"May require swimming";
	case 13: return L"Available 24/7";
	case 14: return L"Recommended at night";
	case 15: return L"Available in winter";
	case 17: return L"Poisonous plants";
	case 18: return L"Dangerous animals";
	case 19: return L"Ticks";
	case 20: return L"Abandoned mine";
	case 21: return L"Cliff/falling rocks";
	case 22: return L"Hunting area";
	case 23: return L"Dangerous area";
	case 24: return L"Wheelchair accessible";
	case 25: return L"Parking nearby";
	case 26: return L"Public transportation nearby";
	case 27: return L"Drinking water nearby";
	case 28: return L"Public restrooms nearby";
	case 29: return L"Telephone nearby";
	case 30: return L"Picnic tables nearby";
	case 31: return L"Camping nearby";
	case 32: return L"Bicycles";
	case 33: return L"Motorcycles";
	case 34: return L"Quads";
	case 35: return L"Off-road vehicles";
	case 36: return L"Snowmobiles";
	case 37: return L"Horses";
	case 38: return L"Campfires";
	case 39: return L"Thorns";
	case 40: return L"Stealth required";
	case 41: return L"Stroller accessible";
	case 42: return L"Needs maintenance";
	case 43: return L"Livestock nearby";
	case 44: return L"Flashlight required";
	case 46: return L"Trucks/RVs";
	case 47: return L"Field puzzle";
	case 48: return L"UV light required";
	case 49: return L"May require snowshoes";
	case 50: return L"May require cross country skis";
	case 51: return L"Special tool required";
	case 52: return L"Night cache";
	case 53: return L"Park and grab";
	case 54: return L"Abandoned structure";
	case 55: return L"Short hike (&lt;1 km)";
	case 56: return L"Medium hike (1 km-10 km)";
	case 57: return L"Long hike (&gt;10 km)";
	case 58: return L"Fuel nearby";
	case 59: return L"Food nearby";
	case 60: return L"Wireless beacon";
	case 61: return L"Partnership cache";
	case 62: return L"Seasonal access";
	case 63: return L"Recommended for tourists";
	case 64: return L"Tree climbing required";
	case 65: return L"Yard (private residence)";
	case 66: return L"Teamwork cache";
	case 67: return L"GeoTour";
	case 69: return L"Bonus cache";
	case 70: return L"Power trail";
	case 71: return L"Challenge cache";
	case 72: return L"Geochecking.com solution checker enabled";
	default: return std::to_wstring(id);
	}
}
