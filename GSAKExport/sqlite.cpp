module;

#include <format>
#include <iostream>
#include <iterator>
#include <sstream>

#include <winsqlite/winsqlite3.h>

#include <gsl/util>

module sqlite;

sqlite::sqlite(std::filesystem::path const& filename)
{
	check_sqlite_return_code(*this, sqlite3_open16(filename.u16string().c_str(), &_db), "sqlite3_open16");
}

sqlite::~sqlite() noexcept
{
	if (int const ret{ sqlite3_close(_db) }; ret != SQLITE_OK) {
		[[gsl::suppress(f .6)]] std::wcerr << sqlite_exception{ *this, ret, "sqlite3_close" }.what() << std::endl;
	}
}

sqlite_stmt::sqlite_stmt(sqlite const& db, std::wstring const& sql)
	: _db{ &db }
{
	int const ret{ sqlite3_prepare16_v2(db, sql.c_str(), gsl::narrow_cast<int>(sql.size() * sizeof(sql[0])), &_stmt, nullptr) };
	check_sqlite_return_code(db, ret, "sqlite3_prepare_v2");
}

sqlite_stmt::~sqlite_stmt() noexcept
{
	if (int const ret{ sqlite3_finalize(_stmt) }; ret != SQLITE_OK) {
		[[gsl::suppress(f .6)]] std::wcerr << sqlite_exception{ *_db, ret, "sqlite3_finalize" }.what() << std::endl;
	}
}

bool sqlite_stmt::step() const
{
	int const ret{ sqlite3_step(_stmt) };
	check_sqlite_return_code(*_db, ret, "sqlite3_step");
	return ret == SQLITE_ROW;
}

std::string sqlite_exception::get_str(sqlite const& db, int result, std::string_view function)
{
	std::ostringstream strm;
	int const errcode{ static_cast<sqlite3*>(db) ? db.errcode() : result };
	int const exterrcode{ static_cast<sqlite3*>(db) ? db.errcode() : result };
	char const* const errmsgstr{ static_cast<sqlite3*>(db) ? sqlite3_errmsg(db) : sqlite3_errstr(result) };
	[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
	std::format_to(std::ostreambuf_iterator{ strm }, "sqlite error {} ({}) in {}: {}", errcode, exterrcode, function, errmsgstr);
	return strm.str();
}

std::wstring sqlite3_column_wstring(sqlite3_stmt* stmt, int iCol)
{
	[[gsl::suppress(type .1)]] return reinterpret_cast<wchar_t const*>(sqlite3_column_text16(stmt, iCol));
}

void check_sqlite_return_code(sqlite const& db, int result, std::string_view function)
{
	if (result != SQLITE_OK && result != SQLITE_DONE && result != SQLITE_ROW) {
		throw sqlite_exception{ db, result, function };
	}
}
