module;

#include <string>
#include <vector>

export module geocache;

export struct geocache_attribute {
	int id;
	int inc;

	std::wstring id_as_string() const;
};

export struct geocache_log {
	int id{};
	std::wstring date_time;
	std::wstring type;
	std::wstring by;
	std::wstring text;
	std::wstring lat;
	std::wstring lon;
	bool encoded{};
};

export struct geocache {
	std::wstring code;
	std::wstring name;
	std::wstring short_desc;
	std::wstring long_desc;
	std::wstring hint;
	std::wstring latitude;
	std::wstring longitude;
	std::wstring placed_by;
	std::wstring type;
	std::wstring container;
	std::wstring id;
	double difficulty{};
	double terrain{};
	bool short_desc_is_html{};
	bool long_desc_is_html{};
	bool available{};
	std::wstring first_log_prefix;
	std::wstring first_log_gcnote;
	std::vector<geocache_attribute> attributes;
	std::vector<geocache_log> last_logs;
	std::vector<geocache_log> my_logs;
};
