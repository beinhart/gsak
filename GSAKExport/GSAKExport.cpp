#include <algorithm>
#include <array>
#include <bit>
#include <cassert>
#include <filesystem>
#include <format>
#include <fstream>
#include <iostream>
#include <iterator>
#include <ranges>
#include <source_location>
#include <span>
#include <sstream>
#include <stdexcept>
#include <string_view>
#include <variant>
#include <vector>

#define NOMINMAX
#include <comdef.h>

#include <Windows.h>

#include <winsqlite/winsqlite3.h>

#include <gsl/narrow>
#include <gsl/span>

#include "gpxfile.h"

import geocache;
import gpilib;
import poi_export_config;
import poi_group;
import point_of_interest;
import sqlite;

using namespace std::string_view_literals;
namespace fs = std::filesystem;
namespace rs = std::ranges;

static_assert(std::endian::native == std::endian::little, "The code for loading a bmp file currently only works on little endian platforms.");

namespace {
	std::wstring replace_all(std::wstring str, std::wstring const& substr, std::wstring const& replacestr)
	{
		size_t pos{};
		while ((pos = str.find(substr, pos)) != std::wstring::npos) {
			str.replace(pos, substr.size(), replacestr);
			pos += replacestr.size();
		}
		return str;
	}

	void read_caches_from_db(sqlite const& db, std::vector<geocache>& geocaches)
	{
		std::wstring const sql{
			LR"(SELECT caches.Code, Name, ShortDescription, LongDescription, Hints, Latitude, Longitude, ShortHtm, LongHtm, PlacedBy, CacheType, Container, Difficulty, Terrain, CacheId, TempDisabled,
	Country||'/'||State||'/'||County||'<br/>'||FavPoints||' favs on '||NumberOfLogs||' logs<br/>Placed: '||PlacedDate||'<br/>'||(CASE HasTravelBug WHEN 1 THEN 'has TB(s)<br/>' ELSE '' END)||'LastFound: '||LastFoundDate,
	GcNote
FROM caches
JOIN cachememo ON cachememo.Code=caches.Code
WHERE NOT Archived AND NOT Found AND NOT IsOwner
ORDER BY caches.Code ASC)"
		};
		sqlite_stmt stmt{ db, sql };
		int i{};
		while (stmt.step()) {
#ifndef NDEBUG
			int type{};
#endif
			assert((type = sqlite3_column_type(stmt, 0)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 1)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 2)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 3)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 4)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 5)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 6)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 7)) == SQLITE_INTEGER);
			assert((type = sqlite3_column_type(stmt, 8)) == SQLITE_INTEGER);
			assert((type = sqlite3_column_type(stmt, 9)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 10)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 11)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 12)) == SQLITE_FLOAT);
			assert((type = sqlite3_column_type(stmt, 13)) == SQLITE_FLOAT);
			assert((type = sqlite3_column_type(stmt, 14)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 15)) == SQLITE_INTEGER);
			assert((type = sqlite3_column_type(stmt, 16)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 17)) == SQLITE_TEXT);

			geocache cache{
				.code				= sqlite3_column_wstring(stmt, 0),
				.name				= sqlite3_column_wstring(stmt, 1),
				.short_desc			= sqlite3_column_wstring(stmt, 2),
				.long_desc			= sqlite3_column_wstring(stmt, 3),
				.hint				= sqlite3_column_wstring(stmt, 4),
				.latitude			= sqlite3_column_wstring(stmt, 5),
				.longitude			= sqlite3_column_wstring(stmt, 6),
				.placed_by			= sqlite3_column_wstring(stmt, 9),
				.type				= sqlite3_column_wstring(stmt, 10),
				.container			= sqlite3_column_wstring(stmt, 11),
				.id					= sqlite3_column_wstring(stmt, 14),
				.difficulty			= sqlite3_column_double(stmt, 12),
				.terrain			= sqlite3_column_double(stmt, 13),
				.short_desc_is_html = gsl::narrow_cast<bool>(sqlite3_column_int(stmt, 7)),
				.long_desc_is_html	= gsl::narrow_cast<bool>(sqlite3_column_int(stmt, 8)),
				.available			= !sqlite3_column_int(stmt, 15),
				.first_log_prefix	= sqlite3_column_wstring(stmt, 16),
				.first_log_gcnote	= replace_all(sqlite3_column_wstring(stmt, 17), L"\n", L"<br/>")
			};
			geocaches.emplace_back(std::move(cache));
			++i;
		}
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ std::wcout }, L"read {} geocaches\n", i);
	}

	void read_logs_from_db(sqlite const& db, std::vector<geocache>& geocaches)
	{
		std::wstring const sql{
			LR"(SELECT Logs.lParent, Logs.lLogId, lDate, lTime, lType, lBy, lLat, lLon, lEncoded, lText
FROM Logs
JOIN Caches ON Logs.lParent=Code
JOIN LogMemo ON LogMemo.lLogId=Logs.lLogId
WHERE NOT Archived AND NOT Found AND NOT IsOwner
ORDER BY Logs.lParent ASC, lDate DESC, lTime DESC, Logs.lLogId DESC)"
		};
		sqlite_stmt stmt{ db, sql };
		size_t logs_all{};
		size_t logs_read{};
		auto iter_cache{ begin(geocaches) };
		while (stmt.step()) {
#ifndef NDEBUG
			int type{};
#endif
			assert((type = sqlite3_column_type(stmt, 0)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 1)) == SQLITE_INTEGER);
			assert((type = sqlite3_column_type(stmt, 2)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 3)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 4)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 5)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 6)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 7)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 8)) == SQLITE_INTEGER);
			assert((type = sqlite3_column_type(stmt, 9)) == SQLITE_TEXT);

			std::wstring const code{ sqlite3_column_wstring(stmt, 0) };
			if (code != iter_cache->code) {
				++iter_cache;
			}
			geocache& cache{ *iter_cache };
			std::wstring by{ sqlite3_column_wstring(stmt, 5) };
			constexpr size_t limit_number_of_logs{ 5 };
			auto const add_log{ [&by](sqlite_stmt& st, std::vector<geocache_log>& v) {
				geocache_log log{
					.id		   = sqlite3_column_int(st, 1),
					.date_time = sqlite3_column_wstring(st, 2) + L'T' + sqlite3_column_wstring(st, 3) + L'Z',
					.type	   = sqlite3_column_wstring(st, 4),
					.by		   = std::move(by),
					.text	   = sqlite3_column_wstring(st, 9),
					.lat	   = sqlite3_column_wstring(st, 6),
					.lon	   = sqlite3_column_wstring(st, 7),
					.encoded   = gsl::narrow_cast<bool>(sqlite3_column_int(st, 8)),
				};
				v.emplace_back(std::move(log));
			} };
			if (cache.last_logs.size() < limit_number_of_logs) {
				add_log(stmt, cache.last_logs);
				++logs_read;
			}
			else if (by == L"beinhart") {
				add_log(stmt, cache.my_logs);
				++logs_read;
			}
			++logs_all;
		}
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ std::wcout }, L"read {} logs of {}\n", logs_read, logs_all);
	}

	void read_attributes_from_db(sqlite const& db, std::vector<geocache>& geocaches)
	{
		std::wstring const sql{
			LR"(SELECT aCode, aId, aInc
FROM Attributes
JOIN Caches ON aCode=Code
WHERE NOT Archived AND NOT Found AND NOT IsOwner
ORDER BY aCode ASC, aId ASC)"
		};
		sqlite_stmt stmt{ db, sql };
		size_t count{};
		auto iter_cache{ begin(geocaches) };
		while (stmt.step()) {
#ifndef NDEBUG
			int type{};
#endif
			assert((type = sqlite3_column_type(stmt, 0)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 1)) == SQLITE_INTEGER);
			assert((type = sqlite3_column_type(stmt, 2)) == SQLITE_INTEGER);

			std::wstring const code{ sqlite3_column_wstring(stmt, 0) };
			if (code != iter_cache->code) {
				++iter_cache;
			}
			geocache& cache{ *iter_cache };
			cache.attributes.emplace_back(geocache_attribute{ .id = sqlite3_column_int(stmt, 1), .inc = sqlite3_column_int(stmt, 2) });
			++count;
		}
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ std::wcout }, L"read {} attributes\n", count);
	}

	std::vector<geocache> read_caches_from_db(sqlite const& db)
	{
		std::vector<geocache> geocaches;
		read_caches_from_db(db, geocaches);
		read_logs_from_db(db, geocaches);
		read_attributes_from_db(db, geocaches);

		return geocaches;
	}

	void read_pois_caches_from_db(sqlite const& db, poi_export_config const& pec, std::vector<poi_group>& poi_groups)
	{
		struct poi_group_sql {
			poi_group group;		 // The poi_group for the results
			std::wstring sql_cond;	 // The SQL expression for checking if to include a cache
			std::wstring sql_text;	 // The SQL expression for the text to set as the POI's text
		};

		std::array<poi_group_sql, 13> groups{ {
			{ { pec.get_poi_groups_gc(L"TravelBug"), L"T " },
			  L"HasTravelBug",
			  L"''" },
			{ { pec.get_poi_groups_gc(L"Favorites"), L"F " },
			  L"NOT Found AND FavPoints > NumberOfLogs * 20 / 100",
			  L"FavPoints||' favs on '||NumberOfLogs||' logs'||CASE NumberOfLogs WHEN 0 THEN '' ELSE '='||(FavPoints*100/NumberOfLogs)||'%' END" },
			{ { pec.get_poi_groups_gc_found(L""), L"" },
			  L"Found AND NOT TempDisabled",
			  L"Country||'/'||State||'/'||County||x'0a'||FavPoints||' favs on '||NumberOfLogs||' logs'||CASE NumberOfLogs WHEN 0 THEN '' ELSE '='||(FavPoints*100/NumberOfLogs)||'%' END||x'0a'||'Placed: '||PlacedDate||' by '||PlacedBy||x'0a'||'Found by me: '||FoundByMeDate||x'0a'||(CASE HasTravelBug WHEN 1 THEN ('has TB(s)'||x'0a') ELSE '' END)||'LastFound: '||LastFoundDate||(CASE GcNote WHEN '' THEN '' ELSE (x'0a'||'GCNote: '||GcNote) END)" },
			{ { pec.get_poi_groups_gc_found(L"Unavailable"), L"" },
			  L"Found AND TempDisabled",
			  L"Country||'/'||State||'/'||County||x'0a'||FavPoints||' favs on '||NumberOfLogs||' logs'||CASE NumberOfLogs WHEN 0 THEN '' ELSE '='||(FavPoints * 100 / NumberOfLogs)||'%' END||x'0a'||'Placed: '||PlacedDate||' by '||PlacedBy||x'0a'||'Found by me: '||FoundByMeDate||x'0a'||(CASE HasTravelBug WHEN 1 THEN('has TB(s)'||x'0a') ELSE '' END)||'LastFound: '||LastFoundDate||(CASE GcNote WHEN '' THEN '' ELSE(x'0a'||'GCNote: '||GcNote) END)" },
			{ { pec.get_poi_groups_gc(L"Unavailable"), L"U " },
			  L"NOT Found AND TempDisabled",
			  L"''" },
			{ { pec.get_poi_groups_gc_placed(L""), L"" },
			  L"IsOwner",
			  L"Country||'/'||State||'/'||County||x'0a'||FavPoints||' favs on '||NumberOfLogs||' logs'||x'0a'||'Placed: '||PlacedDate||x'0a'||(CASE HasTravelBug WHEN 1 THEN ('has TB(s)'||x'0a') ELSE '' END)||'LastFound: '||LastFoundDate||(CASE GcNote WHEN '' THEN '' ELSE (x'0a'||'GCNote: '||GcNote) END)" },
			{ { pec.get_poi_groups_gc_placed(L"Unavailable"), L"U " },
			  L"IsOwner AND TempDisabled",
			  L"''" },
			{ { pec.get_poi_groups_gc(L"Premium"), L"P " },
			  L"NOT Found AND IsPremium",
			  L"''" },
			{ { pec.get_poi_groups_gc(L"Corrected"), L"C " },
			  L"NOT Found AND NOT IsOwner AND HasCorrected",
			  L"''" },
			{ { pec.get_poi_groups_gc(L"DNF"), L"D " },
			  L"DNF",
			  L"DNFDate" },
			{ { pec.get_poi_groups_gc(L"Climbing"), L"C " },
			  L"EXISTS(SELECT NULL FROM Attributes WHERE aCode=Code AND (aID=3 OR aID=10 OR aID=64) AND aInc=1)",
			  L"''" },
			{ { pec.get_poi_groups_gc(L"Parking"), L"P " },
			  L"EXISTS(SELECT NULL FROM Waypoints WHERE cParent=Code AND cType='Parking Area')",
			  L"''" },
			{ { pec.get_poi_groups_gc(L"Water"), L"W " },
			  L"EXISTS(SELECT NULL FROM Attributes WHERE aCode=Code AND (aID=4 OR aID=5 OR aID=11 OR aID=12) AND aInc=1)",
			  L"''" },
		} };

		std::wstring sql{ L"SELECT Longitude, Latitude, Name\n" };
		for (auto const& g : groups) {
			sql += L", ";
			sql += g.sql_cond;
			sql += L", ";
			sql += g.sql_text;
			sql += L"\n";
		}
		sql += L"FROM caches WHERE NOT Archived";

		sqlite_stmt stmt{ db, sql };
		while (stmt.step()) {
#ifndef NDEBUG
			int type{};
#endif
			assert((type = sqlite3_column_type(stmt, 0)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 1)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 2)) == SQLITE_TEXT);
			constexpr int fixed_columns{ 3 };
			int column{ fixed_columns };
			for ([[maybe_unused]] auto const& g : groups) {
				assert((type = sqlite3_column_type(stmt, column++)) == SQLITE_INTEGER);
				assert((type = sqlite3_column_type(stmt, column++)) == SQLITE_TEXT);
			}

			point_of_interest poi{
				.longitude{ sqlite3_column_wstring(stmt, 0) },
				.latitude{ sqlite3_column_wstring(stmt, 1) },
				.name{ sqlite3_column_wstring(stmt, 2) },
			};
			column = fixed_columns;
			for (auto& g : groups) {
				if (sqlite3_column_int(stmt, column++)) {
					poi.desc = sqlite3_column_wstring(stmt, column);
					g.group.pois.push_back(poi);
				}
				++column;
			}
		}
		for (auto& g : groups) {
			poi_groups.push_back(std::move(g.group));
		}
	}

	void read_pois_waypoints_from_db(sqlite const& db, poi_export_config const& pec, std::vector<poi_group>& poi_groups)
	{
		struct poi_group_sql {
			poi_group group;		 // The poi_group for the results
			std::wstring sql_cond;	 // The SQL expression for checking if to include a waypoint
		};

		std::array<poi_group_sql, 18> groups{ {
			{ { pec.get_poi_groups_gc(L"Final") },
			  L"NOT IsOwner AND NOT Found AND cType='Final Location' AND NOT (cLat=Caches.Latitude AND cLon=Caches.Longitude AND cComment='')" },
			{ { pec.get_poi_groups_gc(L"Reference Point") },
			  L"NOT IsOwner AND NOT Found AND cType='Reference Point'" },
			{ { pec.get_poi_groups_gc(L"Virtual Stage") },
			  L"NOT IsOwner AND NOT Found AND cType='Virtual Stage'" },
			{ { pec.get_poi_groups_gc(L"Physical Stage") },
			  L"NOT IsOwner AND NOT Found AND cType='Physical Stage'" },
			{ { pec.get_poi_groups_gc(L"Parking Area") },
			  L"NOT IsOwner AND NOT Found AND cType='Parking Area'" },
			{ { pec.get_poi_groups_gc(L"Trailhead") },
			  L"NOT IsOwner AND NOT Found AND cType='Trailhead'" },
			{ { pec.get_poi_groups_gc_placed(L"Final") },
			  L"IsOwner AND cType='Final Location' AND NOT (cLat=Caches.Latitude AND cLon=Caches.Longitude AND cComment='')" },
			{ { pec.get_poi_groups_gc_placed(L"Reference Point") },
			  L"IsOwner AND cType='Reference Point'" },
			{ { pec.get_poi_groups_gc_placed(L"Virtual Stage") },
			  L"IsOwner AND cType='Virtual Stage'" },
			{ { pec.get_poi_groups_gc_placed(L"Physical Stage") },
			  L"IsOwner AND cType='Physical Stage'" },
			{ { pec.get_poi_groups_gc_placed(L"Parking Area") },
			  L"IsOwner AND cType='Parking Area'" },
			{ { pec.get_poi_groups_gc_placed(L"Trailhead") },
			  L"IsOwner AND cType='Trailhead'" },
			{ { pec.get_poi_groups_gc_found(L"Final") },
			  L"Found AND cType='Final Location' AND NOT (cLat=Caches.Latitude AND cLon=Caches.Longitude AND cComment='')" },
			{ { pec.get_poi_groups_gc_found(L"Reference Point") },
			  L"Found AND cType='Reference Point'" },
			{ { pec.get_poi_groups_gc_found(L"Virtual Stage") },
			  L"Found AND cType='Virtual Stage'" },
			{ { pec.get_poi_groups_gc_found(L"Physical Stage") },
			  L"Found AND cType='Physical Stage'" },
			{ { pec.get_poi_groups_gc_found(L"Parking Area") },
			  L"Found AND cType='Parking Area'" },
			{ { pec.get_poi_groups_gc_found(L"Trailhead") },
			  L"Found AND cType='Trailhead'" },
		} };

		std::wstring sql{
			LR"(SELECT CASE cLat='0.0' AND cLon='0.0' WHEN 1 THEN Longitude ELSE cLon END
	, CASE cLat='0.0' AND cLon='0.0' WHEN 1  THEN Latitude ELSE cLat END
	, (CASE cLat='0.0' AND cLon='0.0' WHEN 1 THEN 'm' ELSE '' END)||cPrefix||' '||Name
	, TRIM(TRIM(WayPoints.cCode||' '||cName)||x'0a'||cComment,x'0a')
)"
		};
		for (auto const& g : groups) {
			sql += L", ";
			sql += g.sql_cond;
			sql += L"\n";
		}
		sql +=
			LR"(FROM WayPoints
JOIN WayMemo ON WayPoints.cCode = WayMemo.cCode
JOIN Caches ON WayPoints.cParent = Caches.Code
WHERE NOT Archived)";

		sqlite_stmt stmt{ db, sql };
		while (stmt.step()) {
#ifndef NDEBUG
			int type{};
#endif
			assert((type = sqlite3_column_type(stmt, 0)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 1)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 2)) == SQLITE_TEXT);
			assert((type = sqlite3_column_type(stmt, 3)) == SQLITE_TEXT);
			constexpr int fixed_columns{ 4 };
			int column{ fixed_columns };
			for ([[maybe_unused]] auto const& g : groups) {
				assert((type = sqlite3_column_type(stmt, column++)) == SQLITE_INTEGER);
			}

			point_of_interest const poi{
				.longitude{ sqlite3_column_wstring(stmt, 0) },
				.latitude{ sqlite3_column_wstring(stmt, 1) },
				.name{ sqlite3_column_wstring(stmt, 2) },
				.desc{ sqlite3_column_wstring(stmt, 3) },
			};

			column = fixed_columns;
			for (auto& g : groups) {
				if (sqlite3_column_int(stmt, column++)) {
					g.group.pois.push_back(poi);
				}
			}
		}
		for (auto& g : groups) {
			poi_groups.push_back(std::move(g.group));
		}
	}

	std::vector<poi_group> read_pois_from_db(sqlite const& db, poi_export_config const& pec)
	{
		std::vector<poi_group> poi_groups;
		read_pois_caches_from_db(db, pec, poi_groups);
		read_pois_waypoints_from_db(db, pec, poi_groups);
		return poi_groups;
	}

	void export_gpx(std::vector<geocache> const& geocaches, fs::path const& filename)
	{
		gpxfile file;
		IXMLDOMElementPtr const root{ file.prepare(true) };
		file.add(root, geocaches);
		//TODO	file.validate();
		file.save(filename);
	}

	void export_pois_gpx(std::vector<poi_group> const& poi_groups, fs::path const& path)
	{
		for (auto const& group : poi_groups) {
			fs::path const filename{ group.names.get_path_gpx(path) };
			if (group.pois.empty()) {
				[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
				std::format_to(std::ostreambuf_iterator{ std::wcout }, L"No entries, deleting {}\n", filename.generic_wstring());
				fs::remove(filename);
				continue;
			}
			gpxfile file;
			IXMLDOMElementPtr root{ file.prepare(false) };
			for (auto const& poi : group.pois) {
				file.add(root, poi, group.prefix_name);
			}
			[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
			std::format_to(std::ostreambuf_iterator{ std::wcout }, L"Writing {} entries to {}\n", size(group.pois), filename.generic_wstring());
			file.save(filename);
		}
	}

	std::unique_ptr<gpilib::Node> load_bitmap(fs::path const& path_images, poi_group_and_subgroup_names const& names, std::uint16_t index)
	{
		// https://docs.microsoft.com/en-us/windows/win32/gdi/bitmap-storage
		// https://de.wikipedia.org/wiki/Windows_Bitmap
		using namespace gpilib;

		fs::path const path{
			path_images /
			[names]() {
				fs::path result{ names.group };
				if (names.has_subgroup() && names.subgroup != names.group) {
					result += L' ';
					result += names.subgroup;
				}
				result += L".bmp";
				return result;
			}()
		};
		if (!fs::exists(path)) {
			[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
			std::format_to(std::ostreambuf_iterator{ std::wcerr }, L"Bitmap file {} not found\n", path.generic_wstring());
			return {};
		}
		std::ifstream infile;
		infile.exceptions(std::ios::failbit | std::ios::badbit);
		infile.open(path, std::ios::binary);
		BITMAPFILEHEADER bfh{};
		[[gsl::suppress(type .1)]]	 // Don't use reinterpret_cast (type.1).
		infile.read(reinterpret_cast<char*>(&bfh), sizeof(bfh));
		if (LOBYTE(bfh.bfType) != 'B' || HIBYTE(bfh.bfType) != 'M'
			|| bfh.bfSize != fs::file_size(path)
			|| bfh.bfReserved1 != 0
			|| bfh.bfReserved2 != 0
			|| bfh.bfOffBits < sizeof(bfh)
			|| bfh.bfSize <= bfh.bfOffBits) {
			throw std::runtime_error{ "bmp file has invalid BITMAPFILEHEADER" };
		}
		BITMAPINFOHEADER bih{};
		//bih.biSizeImage
		//DWORD biClrUsed;
		//DWORD biClrImportant
		[[gsl::suppress(type .1)]]	 // Don't use reinterpret_cast (type.1).
		infile.read(reinterpret_cast<char*>(&bih), sizeof(bih));
		std::size_t const max_size_palette{ bih.biBitCount <= 8 ? std::size_t{ 1 } << bih.biBitCount : std::size_t{} };
		if (bih.biSize < sizeof(bih) || bih.biWidth <= 0
			// Height < 0 is allowed and says that the image is vertically mirrored, but I don't support it because I haven't tested it (yet).
			|| bih.biHeight <= 0
			// Bitcount 16 is also valid, but I don't support it because I haven't tested it (yet).
			|| (bih.biBitCount != 1 && bih.biBitCount != 2 && bih.biBitCount != 8 && bih.biBitCount != 24 && bih.biBitCount != 32)
			|| bih.biPlanes != 1
			|| bih.biCompression != BI_RGB
			|| bih.biClrUsed > max_size_palette) {
			throw std::runtime_error{ "bmp file has invalid BITMAPINFOHEADER" };
		}
		// Header can be longer, could also be a BITMAPV4HEADER or BITMAPV5HEADER.
		infile.ignore(sizeof(bih) - bih.biSize);
		std::size_t const size_palette{ bih.biClrUsed ? bih.biClrUsed : max_size_palette };
		auto const stride{ (bih.biWidth * bih.biBitCount + 31) / 8 / 4 * 4 };
		auto const size_pixels{ abs(stride * bih.biHeight) };
		if (bfh.bfOffBits != sizeof(bfh) + bih.biSize + size_palette * sizeof(RGBQUAD)
			|| std::cmp_less(bfh.bfSize - bfh.bfOffBits, size_pixels)) {
			throw std::runtime_error{ "bmp file has invalid BITMAPFILEHEADER" };
		}

		DataBmp data_bmp{};
		data_bmp.index	  = index;
		data_bmp.width	  = gsl::narrow<std::uint16_t>(bih.biWidth);
		data_bmp.height	  = gsl::narrow<std::uint16_t>(bih.biHeight);
		data_bmp.bitcount = bih.biBitCount;
		data_bmp.unknown  = 1;	 // TODO test 0
		data_bmp.palette.reserve(size_palette);
		for (std::size_t i{}; i < size_palette; ++i) {
			RGBQUAD rq{};
			[[gsl::suppress(type .1)]]	 // Don't use reinterpret_cast (type.1).
			infile.read(reinterpret_cast<char*>(&rq), sizeof(rq));
			data_bmp.palette.emplace_back(rq.rgbRed, rq.rgbGreen, rq.rgbBlue);
		}
		data_bmp.pixels.resize(size_pixels);
		for (std::size_t h = bih.biHeight; h-- > 0;) {
			infile.read(&data_bmp.pixels.at(h * stride), stride);
		}

		infile.close();
		return std::make_unique<Node>(std::move(data_bmp));
	}

	void export_pois_gpi(std::vector<poi_group> poi_groups, fs::path const& path_gpi, fs::path const& path_images)
	{
		using namespace gpilib;

		// Remove all empty poi_groups.
		std::erase_if(poi_groups, [](poi_group const& pg) noexcept { return pg.pois.empty(); });
		// Sort poi_groups by their names to make work easier.
		rs::sort(poi_groups, {}, &poi_group::names);

		if (poi_groups.empty()) {
			[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
			std::format_to(std::ostreambuf_iterator{ std::wcout }, L"No entries, deleting {}\n", path_gpi.generic_wstring());
			fs::remove(path_gpi);
			return;
		}

		std::string const name_gpi{ path_gpi.stem().generic_string() };
		Gpi gpi;
		gpi.nodes.emplace_back(DataStart{ std::chrono::system_clock::now(), name_gpi, std::make_unique<Node>(DataStartExt{ std::array<std::uint8_t, 5>{ 0, 0, 0, 0, 0 } }) });	 //TODO meaningful values for StartExt?
		gpi.nodes.emplace_back(DataCodepage{ 1252, std::make_unique<Node>(DataLicense{ LocalizedString{ name_gpi }, LocalizedString{ "License" } }) });
		std::size_t count_pois{};
		for (auto iter{ begin(poi_groups) }; iter != end(poi_groups);) {
			auto const iter_next_group{ std::upper_bound(iter, end(poi_groups), iter->names.group, [](std::wstring const& gn, poi_group const& pg) noexcept { return gn < pg.names.group; }) };
#ifdef _DEBUG
			auto const elems_in_group{ std::distance(iter, iter_next_group) };
			[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
			std::format_to(std::ostreambuf_iterator{ std::wcout }, L"{} elems with group {}\n", elems_in_group, iter->names.group);
#endif
			gpi.nodes.emplace_back(DataGroup{ LocalizedString{ iter->names.group } });
			auto& data_group{ std::get<DataGroup>(gpi.nodes.back().data) };
			data_group.area = std::make_unique<Node>(DataArea{});
			auto& data_area{ std::get<DataArea>(data_group.area->data) };
			std::vector<std::unique_ptr<Node>> bmps;
			std::uint16_t index_subgroup{};
			for (auto const& pg : rs::subrange{ iter, iter_next_group }) {
#ifdef _DEBUG
				std::wcout << "  " << pg.names << '\n';
#endif
				std::unique_ptr<Node> node_bmp{ load_bitmap(path_images, pg.names, index_subgroup) };
				DataIndexBmp const data_index_bmp{ node_bmp ? std::get<DataBmp>(node_bmp->data).index : std::uint16_t{} };
				for (auto const& poi : pg.pois) {
					data_area.nodes.push_back(std::make_unique<Node>(DataPoi{ LocalizedString{ poi.name }, Coords{ latitude(poi.latitude), longitude(poi.longitude) } }));
					++count_pois;
					auto& data_poi{ std::get<DataPoi>(data_area.nodes.back()->data) };
					if (pg.names.has_subgroup()) {
						data_poi.nodes.push_back(std::make_unique<Node>(DataIndexSubgroup{ index_subgroup }));
					}
					else if (node_bmp) {
						data_poi.nodes.push_back(std::make_unique<Node>(data_index_bmp));
					}
					if (!poi.desc.empty()) {
						data_poi.nodes.push_back(std::make_unique<Node>(DataCmt{ LocalizedString{ poi.desc } }));
					}
					if (data_poi.has_proximity()) {
						data_poi.flags |= DataPoi::Flag_has_proximity;
					}
				}
				if (pg.names.has_subgroup()) {
					std::unique_ptr<Node> node_index_bmp;
					if (node_bmp) {
						node_index_bmp = std::make_unique<Node>(data_index_bmp);
					}
					data_group.metadata.push_back(std::make_unique<Node>(DataSubgroup{ index_subgroup, LocalizedString{ pg.names.subgroup }, std::move(node_index_bmp) }));
				}
				if (node_bmp) {
					bmps.push_back(std::move(node_bmp));
				}
				++index_subgroup;
			}
			data_area.recalculate_area();
			data_area.recalculate_flags();
			AreaSplittingNTree{}.split(*data_group.area);
			// TODO move all pois with proximity to their own area or keep them together with the other pois?
			rs::move(bmps, std::back_inserter(data_group.metadata));
			iter = iter_next_group;
		}
		gpi.nodes.emplace_back(DataEof{});
		std::ofstream os;
		os.exceptions(std::ios::failbit | std::ios::badbit);
		os.open(path_gpi, std::ios::binary);
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ std::wcout }, L"Writing {} entries to {}\n"sv, count_pois, path_gpi.generic_wstring());
		gpi.write(os);
	}

	void export_pois(std::vector<poi_group> poi_groups, poi_export_config const& pec)
	{
		if (pec.path_gpx.has_value()) {
			export_pois_gpx(poi_groups, pec.path_gpx.value());
		}
		if (pec.path_gpi.has_value()) {
			export_pois_gpi(std::move(poi_groups), pec.path_gpi.value(), pec.path_images.value());
		}
	}
}	// namespace

void usage()
{
	std::wcerr << LR"(Name
    gsakexport

Synopsis
    gsakexport gsakdb options

Description
    gsakdb    The path of the GSAK database file to read.

Options
    --caches exportfile    Export all caches to the GPX file exportfile.
    --pois-gpx path        Export all POIs as gpx files to the path.
    --pois-gpi gpifile     Export all POIs as gpi file to the file gpifile. Requires --images.
    --images imagepath     When exporting all POIs as gpi file then read the bmp files for the POIs from imagepath.
                           The following files will be read:
                           GC Climbing.bmp
                           GC Corrected.bmp
                           GC DNF.bmp
                           GC Favorites.bmp
                           GC Final.bmp
                           GC Parking Area.bmp
                           GC Parking.bmp
                           GC Physical Stage.bmp
                           GC Premium.bmp
                           GC Reference Point.bmp
                           GC Trailhead.bmp
                           GC TravelBug.bmp
                           GC Unavailable.bmp
                           GC Virtual Stage.bmp
                           GC Water.bmp
                           GC_Found Final.bmp
                           GC_Found Parking Area.bmp
                           GC_Found Physical Stage.bmp
                           GC_Found Reference Point.bmp
                           GC_Found Trailhead.bmp
                           GC_Found Unavailable.bmp
                           GC_Found Virtual Stage.bmp
                           GC_Found.bmp
                           GC_Placed Final.bmp
                           GC_Placed Parking Area.bmp
                           GC_Placed Physical Stage.bmp
                           GC_Placed Reference Point.bmp
                           GC_Placed Trailhead.bmp
                           GC_Placed Unavailable.bmp
                           GC_Placed Virtual Stage.bmp
                           GC_Placed.bmp
    --subfolder            Will put all POIs for not found and not placed caches into a subfolder. The subfolder
                           must already exist.
    --subfolder-found      Will put all POIs for found caches into a subfolder. The subfolder must already exist.
    --subfolder-placed     Will put all POIs for placed caches into a subfolder. The subfolder must already exist.
    --prefix pre           The prefix for all POIs for not found and not owned caches. Defaults to "GC".
    --prefix-found pre     The prefix for all POIs for found caches. Defaults to "GC_Found".
    --prefix-found pre     The prefix for all POIs for placed caches. Defaults to "GC_Placed".
                           The prefixes will either be prepended as "pre " before the name or if subfolders
                           are to be used then as "pre/".
                           If you change a prefix, then this also changes the name of the bmp files that will
                           be used.
)";
}

int wmain(int argc, wchar_t** const argv_)
try {
	using namespace std::string_view_literals;

	gsl::span<wchar_t*> const argv(argv_, argc);
	if (argv.size() < 2) {
		usage();
		return std::source_location::current().line();
	}
	fs::path path_db{ gsl::at(argv, 1) };
	std::optional<fs::path> option_caches;
	poi_export_config pec;
	for (auto iter{ std::begin(argv) + 2 }; iter != std::end(argv); ++iter) {
		if (*iter == L"--caches"sv && ++iter != std::end(argv)) {
			option_caches = *iter;
		}
		else if (*iter == L"--pois-gpx"sv && ++iter != std::end(argv)) {
			pec.path_gpx = *iter;
		}
		else if (*iter == L"--pois-gpi"sv && ++iter != std::end(argv)) {
			pec.path_gpi = *iter;
		}
		else if (*iter == L"--images"sv && ++iter != std::end(argv)) {
			pec.path_images = *iter;
		}
		else if (*iter == L"--subfolder"sv) {
			pec.subfolder_gc = true;
		}
		else if (*iter == L"--subfolder-found"sv) {
			pec.subfolder_gc_found = true;
		}
		else if (*iter == L"--subfolder-placed"sv) {
			pec.subfolder_gc_placed = true;
		}
		else if (*iter == L"--prefix"sv && ++iter != std::end(argv)) {
			pec.prefix_gc = *iter;
		}
		else if (*iter == L"--prefix-found"sv && ++iter != std::end(argv)) {
			pec.prefix_gc_found = *iter;
		}
		else if (*iter == L"--prefix-placed"sv && ++iter != std::end(argv)) {
			pec.prefix_gc_placed = *iter;
		}
		else {
			usage();
			return std::source_location::current().line();
		}
	}
	if (pec.path_gpi.has_value() && !pec.path_images.has_value()) {
		std::wcerr << L"--pois-gpi requires --images\n";
		return std::source_location::current().line();
	}

	com_initialize com;
	sqlite db{ path_db };
	if (pec.path_gpx.has_value() || pec.path_gpi.has_value()) {
		auto poi_groups{ read_pois_from_db(db, pec) };
		export_pois(std::move(poi_groups), pec);
	}
	if (option_caches.has_value()) {
		auto const geocaches{ read_caches_from_db(db) };
		export_gpx(geocaches, option_caches.value());
	}

	return 0;
}
catch (std::exception const& e) {
	std::wcerr << L"exception occured: " << e.what() << L'\n';
}
catch (_com_error const& e) {
	[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
	std::format_to(std::ostreambuf_iterator{ std::wcerr }, L"com exception occured: {}\n", e.ErrorMessage());
}
