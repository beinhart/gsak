# General
After using GSAK for years and ending up with a database with 9000 caches and 2.7 million logs exporting to my Garmin device with the macro
[GarminExport.gsk](https://gsak.net/board/index.php?showtopic=7745) now takes incredibly long 35 minutes. This splits into 27 minutes for GPX export,
1 minute for GPX to GGZ conversion and 7 minutes for POI export. That makes it hard to use the macro on a daily basis when being on a geocaching
holiday.

So I wrote this little tool `GSAKExport` that replaces the GPX and POI export. The tool is configurable and can write all caches to a GPX file
and all POIs either to one GPX file per POI type or to one GPI file. The caches GPX can either be directly copied to the GPS device, or better
be converted to GGZ (by GSAK) and then copied to the GPS device. The POI GPX files also need to be converted to POI, for which Garmin's `POILoader`
can be used. For an overall process look at [`macros/GarminExport - whe.gsk`](macros/GarminExport - whe.gsk). This macro is running `GSAKExport`
and then uses GSAK's function `MakeGGZ` to convert the caches GPX file to GGZ.

## Differences between `GarminExport.gsk` and `GSAKExport`+`GarminExport - whe.gsk`

|What|`GarminExport.gsk`|`GSAKExport`+`GarminExport - whe.gsk`|Comment|
|-|-|-|-|
|Configurability|Very flexible|Limited flexibility|If desired, `GSAKExport` could easily be extended/adjusted|
|Tested on|Different devices from different manufactures|GPSmap 64s||
|Runtime|35 minutes on my database (already including [this fix](https://gsak.net/board/index.php?act=ST&f=7&t=15866&hl=&view=findpost&p=235683))|<4:30 minutes on my database|Exporting the caches takes seconds, adding the logs takes >90% of the time. So purging the logs would help, but that is not what I want in my database.|
|Geocache GPX file|Very much information|Stripped down to what the GPSmap 64s displays (no bloat)||
|First log on a cache|As configured|Fixed, more information (Country, State, County, FavPoints, NumberOfLogs, PlacedDate, HasTravelBug, LastFoundDate, GcNote, Attributes)||
|Waypoint POIs|At given coordinates|Name prefixed with waypoint prefix (e.g. "01 " or "FI "). If no coordinates given, then moved to cache coordinates and name prepended with 'm'.||
|Attribute POIs|Same name as geocache. On GPS device sometimes confusing to get the right entry when a geocache and 2 attributes are at the same coordinates.|Name prefixed with a letter for the attribute type (e.g. "P name").||
|Found Caches Child Points POIs|Only geocaches|Geocaches + selected attributes||
|Placed Caches Child Points POIs|Only geocaches|Geocaches + selected attributes||
|POIs|17 POI files, displayed under Extras/"User POIs"|1 POI file, displayed under Extras/GSAKExport|[GPSmap 64s can only handle 20 POI files and I already have about half a dozen downloaded POI files on my device.](https://gsak.net/board/index.php?act=ST&f=7&t=15866&hl=&view=findpost&p=236337)|

## Mapping `GarminExport.gsk` GUI configuration settings to `GSAKExport`:

Currently `GSAKExport` and `GarminExport - whe.gsk` come without GUI, so settings need to be changed directly in the code.

|`GarminExport.gsk`|`GSAKExport`|`GarminExport - whe.gsk`|Comment|
|-|-|-|-|
|Selection of drive letter|Command line parameter|GPX written to GSAK temp folder, GGZ written to `GPSInfo("GarminDrive") + "\Garmin\GGZ\GSAK.ggz"`||
|GPX or GGZ|GPX only|Write GPX in temp folder, then convert to GGZ and write to GPS device||
|Filename/"Use database name as file name?"|Command line parameter|GSAK.GPX/GSAK.ggz||
|"Suppress Status Msgs?"|Always statistics output|No output||
|"Use Current Filter"|-|-||
|"Select Database"|Command line parameter|Database currently in use||
|"Select Location"|- (not relevant)|- (not relevant)||
|"Export Filter"|Hard coded to "not found and not archived and not isowner"|||
|"MFilter (Where)"|-|-||
|"Convert Benchmarks to Geocaches?"|No|-||
|"Convert GSAK created waypoints to Geocaches?"|No|-||
|"Correct Duplicated CacheID#s?"|No|-||
|"Number of Caches"|All caches|-||
|"Number of Logs"|5|-||
|"Always Include My Logs"|Yes|-||
|"Include User Notes"|Yes|-||
|"Use Large Font"|No|-||
|"Add Log Type Text"|Yes|-||
|"Include Attributes"|Yes|-||
|"Include Child Options in GPX"|No|-||
|"Include Child Options in POI"|Yes|-||
|"Use Custom Cache Name"|No|-||
|"Include Attribute POIs"|Yes|-||
|"Include Attribute POIs"/"Parking"|Yes|-||
|"Include Attribute POIs"/"Corrected"|Yes|-||
|"Include Attribute POIs"/"Unavailable"|Yes|-||
|"Include Attribute POIs"/"Micro"|No|-||
|"Include Attribute POIs"/"DNF"|Yes|-||
|"Include Attribute POIs"/"Earthcache"|No|-||
|"Include Attribute POIs"/"Benchmark"|No|-||
|"Include Attribute POIs"/"Travel Bug"|Yes|-||
|"Include Attribute POIs"/"GC Attributes"|Yes (Climbing gear required OR Difficult climb OR Tree climbing required)|||
|"Include Attribute POIs"/"GC Attributes"|Yes (Boat required OR Scuba gear required OR May require wading OR May require swimming)|||
|"Include Attribute POIs"/"favorites"|Yes (20 %)||`GarminExport.gsk` compares to absolute value, `GSAKExport` compares to relative value (FavPoints/NumberOfLogs)|
|"Include Attribute POIs"/"Proximity"|No|||
|"Include Attribute POIs"/"Custom1"|Yes (isPremium)|-||
|"Include Attribute POIs"/"Custom2"|No|-||
|"Custom User Export Macros"|Hard coded (Country, State, County, FavPoints, NumberOfLogs, PlacedDate, HasTravelBug, LastFoundDate, GcNote, Attributes)|-||
|"Found Caches POI Export"/"Export found caches as POIs"|Yes|-||
|"Found Caches POI Export"/"Include Child Points"|Yes|-||
|"Found Caches POI Export"/"Use Transparent Icons"|-|-|`POILoader.exe` uses the icons that are in the POI GPX folder, `GSAKExport` the ones in the images folder|
|"Found Caches POI Export"/"Found Database"|Command line parameter|Database currently in use|Only one database can be given in command line, so no separate databases for default geocaches, found and placed|
|"Found Caches POI Export"/"Export Filter"|-|-||
|"Found Caches POI Export"/"Use Custom POI Name"|-|-||
|"Placed Caches POI Export"/"Export found caches as POIs"|Yes|-||
|"Placed Caches POI Export"/"Include Child Points"|Yes|-||
|"Placed Caches POI Export"/"Use Transparent Icons"|-|-|`POILoader.exe` uses the icons that are in the POI GPX folder, `GSAKExport` the ones in the images folder|
|"Placed Caches POI Export"/"Placed Database"|Command line parameter|Database currently in use|Only one database can be given in command line, so no separate databases for default geocaches, found and placed|
|"Placed Caches POI Export"/"Export Filter"|-|-||
|"Placed Caches POI Export"/"Use Custom POI Name"|-|-||

## Prerequisites
To compile the code you need Visual Studio 2022 or newer, which can be downloaded from [here](https://visualstudio.microsoft.com/de/) for free (select "Visual Studio Community").

My project depends on the open source [Guideline Support Library](https://github.com/microsoft/GSL/).
Clone the branch [master](https://github.com/microsoft/GSL/tree/master) (I tested with [commit 21b69e5](https://github.com/microsoft/GSL/commit/21b69e5ccedb036ea3c0c6275519cbe51a8dbc03)).
