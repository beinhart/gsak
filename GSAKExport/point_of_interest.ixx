module;

#include <string>

export module point_of_interest;

export struct point_of_interest {
	std::wstring longitude;
	std::wstring latitude;
	std::wstring name;
	std::wstring desc;
};
