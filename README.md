# General
This repository contains my tools that are related to GSAK.

# Folder `macros`
My GSAK macros.

# Folder `profiletext`
`profiletext.sh` can be used after the `FindStatGen` macro has run. It will load `profile.txt` and append the `FindStatgGen`statistics to it.
The output will be written to `output.txt` and opened in `Notepad`. There it can be edited and copied to the `geocaching.com` user profile.

# Folder `GSAKExport`
After using GSAK for years and ending up with a database with 9000 caches and 2.7 million logs exporting to my Garmin device with the macro
[GarminExport.gsk](https://gsak.net/board/index.php?showtopic=7745) now takes incredibly long 35 minutes. This splits into 27 minutes for GPX export,
1 minute for GPX to GGZ conversion and 7 minutes for POI export. That makes it hard to use the macro on a daily basis when being on a geocaching
holiday.

So I wrote this little tool `GSAKExport` that replaces the GPX and POI export. The tool is configurable and can write all caches to a GPX file
and all POIs either to one GPX file per POI type or to one GPI file. The caches GPX can either be directly copied to the GPS device, or better
be converted to GGZ (by GSAK) and then copied to the GPS device. The POI GPX files also need to be converted to POI, for which Garmin's `POILoader`
can be used. For an overall process look at [`macros/GarminExport - whe.gsk`](macros/GarminExport - whe.gsk). This macro is running `GSAKExport`
and then uses GSAK's function `MakeGGZ` to convert the caches GPX file to GGZ.
