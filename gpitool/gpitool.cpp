#include <cassert>
#include <cstddef>
#include <exception>
#include <filesystem>
#include <format>
#include <fstream>
#include <iostream>
#include <iterator>
#include <typeinfo>
#include <variant>

#include <gsl/narrow>
#include <gsl/span>

// We are using gsl::span whichs call gsl::details::terminate, so ignore warnings that apply to std::span.
#pragma warning(disable : 26711)   // warning C26711: Potential overflow of span using expression '...'

import gpilib;

using namespace gpilib;

namespace config {
	constexpr bool with_statistics{ true };
	constexpr bool print_gpi{ true };
	constexpr bool write_gpi{ true };
	std::filesystem::path const path_gpi{ "gpitool.output.gpi" };
}	// namespace config

int main(int argc, char** argv)
try {
	std::ios::sync_with_stdio(false);

	auto const args{ gsl::span<char*, gsl::dynamic_extent>{ argv, gsl::narrow<std::size_t>(argc) }.subspan(1) };
	for (auto const& arg : args) {
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ std::cout }, "Processing file {}\n", arg);
		std::ifstream infile;
		infile.exceptions(std::ios::failbit | std::ios::badbit);
		infile.open(arg, std::ios::binary);
		Gpi gpi{};
		if (!(gpi.read(infile))) {
			std::cerr << "Failed to read file\n";
		}
		infile.close();

		if constexpr (config::with_statistics) {
			std::for_each(begin(gpi.nodes) + 2, --end(gpi.nodes), [](Node& node) {
				assert(node.tag == Tag::Group);
				std::get<DataArea>(std::get<DataGroup>(node.data).area->data).update_statistics();
			});
		}

		if constexpr (config::write_gpi) {
			std::ofstream os;
			os.exceptions(std::ios::failbit | std::ios::badbit);
			os.open(config::path_gpi, std::ios::binary);
			gpi.write(os);
			os.close();
		}

		if constexpr (config::print_gpi) {
			std::cout << gpi;
		}
	}

	return 0;
}
catch (std::exception const& e) {
	[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
	std::format_to(std::ostreambuf_iterator{ std::cerr }, "Exception {}: {}\n", typeid(e).name(), e.what());
	return 1;
}
