#pragma once

#include <format>

#include <gsl/util>

namespace std {
	// https://en.cppreference.com/w/cpp/named_req/BasicFormatter
	template <class CharT>
	struct std::formatter<gpilib::Area, CharT> {
		// Parses the format-spec [parse_ctx.begin(), parse_ctx.end()) for type Arg until the first unmatched character.
		// Throws std::format_error unless the whole range is parsed or the unmatched character is }. [note 1]
		// Stores the parsed format specifiers in f and returns an end iterator of the parsed range.
		template <typename FormatParseContext>
		constexpr auto parse(FormatParseContext& fpc)
		{
			// parse formatter args like padding, precision if you support it
			auto iter{ fpc.begin() };
			return iter;   // returns the iterator to the last parsed character in the format string, in this case we just swallow everything
		}

		// Formats arg according to the specifiers stored in f, writes the output to format_ctx.out() and returns an end iterator of the output range.
		// The output shall only depend on
		//	arg,
		//	format_ctx.locale(),
		//	the range [parse_ctx.begin(), parse_ctx.end()) from the last call to f.parse(parse_ctx), and
		//	format_ctx.arg(n) for any value n of type std::size_t.
		template <typename FormatContext>
		auto format(gpilib::Area const& area, FormatContext& fc)
		{
			[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
			return std::format_to(fc.out(), "min={} max={}", area.min, area.max);
		}
	};

	// https://en.cppreference.com/w/cpp/named_req/BasicFormatter
	template <class CharT>
	struct std::formatter<gpilib::Coords, CharT> {
		enum class Type { Degrees,
						  DegreesMinutes };
		// Parses the format-spec [parse_ctx.begin(), parse_ctx.end()) for type Arg until the first unmatched character.
		// Throws std::format_error unless the whole range is parsed or the unmatched character is }. [note 1]
		// Stores the parsed format specifiers in f and returns an end iterator of the parsed range.
		template <typename FormatParseContext>
		constexpr auto parse(FormatParseContext& fpc)
		{
			// parse formatter args like padding, precision if you support it
			auto iter{ fpc.begin() };
			if (iter == fpc.end() || *iter != 'd') {
				return iter;
			}
			_type = Type::Degrees;
			++iter;
			if (iter == fpc.end() || *iter != 'm') {
				return iter;
			}
			_type = Type::DegreesMinutes;
			++iter;
			return iter;   // returns the iterator to the last parsed character in the format string, in this case we just swallow everything
		}

		// Formats arg according to the specifiers stored in f, writes the output to format_ctx.out() and returns an end iterator of the output range.
		// The output shall only depend on
		//	arg,
		//	format_ctx.locale(),
		//	the range [parse_ctx.begin(), parse_ctx.end()) from the last call to f.parse(parse_ctx), and
		//	format_ctx.arg(n) for any value n of type std::size_t.
		template <typename FormatContext>
		auto format(gpilib::Coords const& coord, FormatContext& fc)
		{
			switch (_type) {
			case Type::Degrees: {
				auto const lat_degrees{ coord.latitude.degrees() };
				auto const lon_degrees{ coord.longitude.degrees() };
				[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
				return format_to(fc.out(), "{}{:02.7f} {}{:03.7f}", lat_degrees >= 0 ? 'N' : 'S', abs(lat_degrees), lon_degrees >= 0 ? 'E' : 'W', abs(lon_degrees));
			}
			case Type::DegreesMinutes: {
				auto const [lat_degrees, lat_milliminutes] = coord.latitude.degrees_milliminutes();
				auto const [lon_degrees, lon_milliminutes] = coord.longitude.degrees_milliminutes();
				[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
				return format_to(fc.out(), "{}{:02} {:02}.{:03} {}{:03} {:02}.{:03}",
								 lat_degrees >= 0 ? 'N' : 'S', lat_degrees, lat_milliminutes / 1000, lat_milliminutes % 1000,
								 lon_degrees >= 0 ? 'E' : 'W', lon_degrees, lon_milliminutes / 1000, lon_milliminutes % 1000);
			}
			}
			return fc.out();
		}

		Type _type{ Type::DegreesMinutes };
	};

	// https://en.cppreference.com/w/cpp/named_req/BasicFormatter
	template <class CharT>
	struct std::formatter<gpilib::String, CharT> {
		// Parses the format-spec [parse_ctx.begin(), parse_ctx.end()) for type Arg until the first unmatched character.
		// Throws std::format_error unless the whole range is parsed or the unmatched character is }. [note 1]
		// Stores the parsed format specifiers in f and returns an end iterator of the parsed range.
		template <typename FormatParseContext>
		constexpr auto parse(FormatParseContext& fpc)
		{
			// parse formatter args like padding, precision if you support it
			auto iter{ fpc.begin() };
			return iter;   // returns the iterator to the last parsed character in the format string, in this case we just swallow everything
		}

		// Formats arg according to the specifiers stored in f, writes the output to format_ctx.out() and returns an end iterator of the output range.
		// The output shall only depend on
		//	arg,
		//	format_ctx.locale(),
		//	the range [parse_ctx.begin(), parse_ctx.end()) from the last call to f.parse(parse_ctx), and
		//	format_ctx.arg(n) for any value n of type std::size_t.
		template <typename FormatContext>
		auto format(gpilib::String const& str, FormatContext& fc)
		{
			[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
			return std::format_to(fc.out(), "{}", str.str);
		}
	};

	// https://en.cppreference.com/w/cpp/named_req/BasicFormatter
	template <class CharT>
	struct std::formatter<gpilib::LocalizedString, CharT> {
		// Parses the format-spec [parse_ctx.begin(), parse_ctx.end()) for type Arg until the first unmatched character.
		// Throws std::format_error unless the whole range is parsed or the unmatched character is }. [note 1]
		// Stores the parsed format specifiers in f and returns an end iterator of the parsed range.
		template <typename FormatParseContext>
		constexpr auto parse(FormatParseContext& fpc)
		{
			// parse formatter args like padding, precision if you support it
			auto iter{ fpc.begin() };
			return iter;   // returns the iterator to the last parsed character in the format string, in this case we just swallow everything
		}

		// Formats arg according to the specifiers stored in f, writes the output to format_ctx.out() and returns an end iterator of the output range.
		// The output shall only depend on
		//	arg,
		//	format_ctx.locale(),
		//	the range [parse_ctx.begin(), parse_ctx.end()) from the last call to f.parse(parse_ctx), and
		//	format_ctx.arg(n) for any value n of type std::size_t.
		template <typename FormatContext>
		auto format(gpilib::LocalizedString const& str, FormatContext& fc)
		{
			[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
			return std::format_to(fc.out(), "{}:{}", std::string_view{ begin(str.language), end(str.language) }, str);
		}
	};
}	// namespace std
