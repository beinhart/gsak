module;

#include <algorithm>
#include <array>
#include <bit>
#include <cassert>
#include <chrono>
#include <format>
#include <fstream>
#include <iostream>
#include <iterator>
#include <limits>
#include <numbers>
#include <numeric>
#include <ranges>
#include <stdexcept>
#include <string>
#include <string_view>
#include <utility>
#include <variant>
#include <vector>

#include <gsl/narrow>

#define NOMINMAX
#include <Windows.h>

module gpilib;

#include "gpilib_format.h"

using namespace std::string_view_literals;
namespace nrs = std::numbers;
namespace rs  = std::ranges;
namespace rv  = std::views;

static_assert(std::endian::native == std::endian::little);	 // Currently only little endian is supported.

#pragma warning(disable : 26490)   // Don't use reinterpret_cast (flags.1).

// TODO: custom formatter for our types so that we can use format_to all the time
// TODO: Check order of subnode tags?
// add proximity alarms to GC-POIs?

namespace {
	constexpr bool isPaletteBased(std::uint16_t bitcount)
	{
		return bitcount <= 8;
	}

	constexpr std::uint32_t paletteSize(std::uint16_t bitcount)
	{
		return isPaletteBased(bitcount) ? std::uint32_t{ 1 } << bitcount : 0;
	}

	constexpr std::uint16_t calcStride(std::uint16_t width, std::uint16_t bitcount)
	{
		return gsl::narrow<std::uint16_t>((width * bitcount + 31) / 8 / 4 * 4);
	}

#pragma pack(push)
#pragma pack(1)
	struct FileDataString {
		constexpr FileDataString() noexcept = default;
		explicit constexpr FileDataString(std::size_t len_) noexcept
			: len{ gsl::narrow<std::uint16_t>(len_) }
		{
		}

		std::uint16_t len{};
	};

	struct FileDataLocalizedString {
		constexpr FileDataLocalizedString() noexcept = default;
		constexpr FileDataLocalizedString(std::array<char, 2> language_, std::size_t len_str) noexcept
			: len{ gsl::narrow<std::uint32_t>(sizeof(FileDataLocalizedString) - sizeof(len) + len_str) }
			, language{ language_ }
		{
		}

		std::uint32_t len{ sizeof(FileDataLocalizedString) - sizeof(len) };
		std::array<char, 2> language{};
	};

	struct FileDataStart {
		constexpr FileDataStart() noexcept = default;
		constexpr FileDataStart(bool has_node, std::chrono::system_clock::time_point time_)
			: f1{ has_node ? '1' : '0' }
			, time{ gsl::narrow<std::uint32_t>(std::chrono::system_clock::to_time_t(time_) - time_offset) }
			, f2{ has_node ? std::uint16_t{ 3 } : std::uint16_t{} }
		{
		}

		std::array<char, 7> marker{ 'G', 'R', 'M', 'R', 'E', 'C', '0' };
		char f1{};
		std::uint32_t time{};
		std::uint16_t f2{};

		std::chrono::system_clock::time_point get_time() const noexcept { return std::chrono::system_clock::from_time_t(time + time_offset); }

		static constexpr time_t time_offset = 631065600;   // Offset to add to time to get a time_t.
	};

	struct FileDataCodepage {
		constexpr FileDataCodepage() noexcept = default;
		constexpr FileDataCodepage(bool has_node, std::uint16_t codepage_) noexcept
			: f1{ has_node ? '1' : '0' }
			, codepage{ codepage_ }
			, subtag{ has_node ? gpilib::Tag::License : gpilib::Tag{} }
		{
		}

		std::array<char, 7> marker{ 'P', 'O', 'I', '\0', '\0', '\0', '0' };
		char f1{};
		std::uint16_t codepage{ 1252 };
		gpilib::Tag subtag{};
	};

	struct FileDataPoi {
		constexpr FileDataPoi() noexcept = default;
		constexpr FileDataPoi(gpilib::Coords coords, std::uint16_t flags_) noexcept
			: latitude{ coords.latitude.value }
			, longitude{ coords.longitude.value }
			, flags{ flags_ }
		{
		}

		std::int32_t latitude{};
		std::int32_t longitude{};
		char index{ 1 };
		std::uint16_t flags{};
	};

	struct FileDataProximityAlarm {
		constexpr FileDataProximityAlarm() noexcept = default;
		constexpr FileDataProximityAlarm(char type_, char subtype_, std::uint16_t distance_, std::uint16_t speed_) noexcept
			: distance{ distance_ }
			, speed{ speed_ }
			, subtype{ subtype_ }
			, type{ type_ }
		{
		}

		std::uint16_t distance{};
		std::uint16_t speed{};
		std::array<std::uint8_t, 6> marker1{ 0, 1, 0, 1, 1, 1 };
		char subtype{};
		char type{};
	};

	struct FileDataBmp {
		constexpr FileDataBmp() noexcept = default;
		constexpr FileDataBmp(std::size_t lenNodeHdr, std::uint16_t index_, std::uint16_t width_, [[maybe_unused]] std::uint16_t height_, std::uint16_t bitcount_, std::uint32_t unknown_)
			: index{ index_ }
			, height{ height_ }
			, width{ width_ }
			, stride{ calcStride(width_, bitcount_) }
			, bitcount{ bitcount_ }
			, compression{}
			, sizeImage{ gsl::narrow<std::uint16_t>(height * stride) }
			, offsetImage{ gsl::narrow<std::uint32_t>(lenNodeHdr + sizeof(*this)) }
			, maxPaletteSize{ paletteSize(bitcount) }
			, unknown{ unknown_ }
			, offsetPalette{ isPaletteBased(bitcount) ? gsl::narrow<std::uint32_t>(lenNodeHdr + sizeof(*this) + sizeImage) : std::uint32_t{} }
		{
		}

		// BMP file format: https://de.wikipedia.org/wiki/Windows_Bitmap
		std::uint16_t index{};
		std::uint16_t height{};
		std::uint16_t width{};
		std::uint16_t stride{};
		std::uint16_t bitcount{};
		std::uint16_t compression{};
		std::uint32_t sizeImage{};
		std::uint32_t offsetImage{};   // From start of Node
		std::uint32_t maxPaletteSize{};
		std::uint32_t marker{ 0x00ff00ffu };
		std::uint32_t unknown{};
		std::uint32_t offsetPalette{};	 // From start of Node
	};

	struct FileDataAddress {
		constexpr FileDataAddress() noexcept = default;
		constexpr FileDataAddress(bool has_city, bool has_country, bool has_state, bool has_postcode, bool has_street, bool has_housenr)
			: flags{ gsl::narrow<std::uint16_t>((has_city ? 1 : 0) + (has_country ? 2 : 0) + (has_state ? 4 : 0) + (has_postcode ? 8 : 0) + (has_street ? 0x10 : 0) + (has_housenr ? 0x20 : 0)) }
		{
		}

		std::uint16_t flags{};
	};

	struct FileDataContact {
		constexpr FileDataContact() noexcept = default;
		constexpr FileDataContact(bool has_phone, bool has_mail, bool has_web)
			: flags{ gsl::narrow<std::uint16_t>((has_phone ? 1 : 0) + (has_mail ? 8 : 0) + (has_web ? 0x10 : 0)) }
		{
		}

		std::uint16_t flags{};
	};

	struct FileDataArea {
		constexpr FileDataArea() noexcept = default;
		constexpr FileDataArea(gpilib::Area area, std::uint16_t flags_) noexcept
			: maxLatitude{ area.max.latitude.value }
			, maxLongitude{ area.max.longitude.value }
			, minLatitude{ area.min.latitude.value }
			, minLongitude{ area.min.longitude.value }
			, flags{ flags_ }
		{
		}

		std::int32_t maxLatitude{};
		std::int32_t maxLongitude{};
		std::int32_t minLatitude{};
		std::int32_t minLongitude{};
		std::array<char, 5> marker{ 0, 0, 0, 0, 1 };
		std::uint16_t flags{};
	};

	struct FileDataDesc {
		constexpr FileDataDesc() noexcept = default;
		constexpr FileDataDesc(bool garmin, bool has_desc)
			: flags{ gsl::narrow<std::uint8_t>((garmin ? 4 : 0) + (has_desc ? 1 : 0)) }
		{
		}

		std::uint8_t flags{};
	};

	struct FileDataLicense {
		constexpr FileDataLicense() noexcept = default;

		std::array<char, 8> marker{ 4, 0, 0, 0, 0, 0, 0, 0 };
	};

	struct FileDataMp3_1 {
		constexpr FileDataMp3_1() noexcept = default;
		constexpr FileDataMp3_1(char index_) noexcept
			: index{ index_ }
		{}

		char index{};
		std::array<char, 2> marker{ 0x20, 1 };
	};

	struct FileDataMp3_2 {
		constexpr FileDataMp3_2() noexcept = default;
		constexpr FileDataMp3_2(std::array<char, 2> language_, std::size_t lenMp3_) noexcept
			: len{ gsl::narrow<std::uint32_t>(sizeof(*this) + lenMp3_) }
			, language{ language_ }
			, lenMp3{ gsl::narrow<std::uint32_t>(lenMp3_) }
		{
		}

		std::uint32_t len{};
		std::array<char, 2> language{};
		std::uint32_t lenMp3{};
	};

	struct FileDataNode {
		gpilib::Tag tag{};
		std::uint16_t flags{};
		std::uint32_t len{};
	};
#pragma pack(pop)
};	 // namespace

namespace gpilib {

	namespace {
		[[nodiscard]] bool node_tags_are_unique(std::vector<std::unique_ptr<Node>> const& nodes)
		{
			std::vector<Tag> tags(size(nodes));
			rs::transform(nodes, begin(tags), [](std::unique_ptr<Node> const& node) noexcept { return node->tag; });
			rs::sort(tags);
			return rs::adjacent_find(tags) == end(tags);
		}

		[[nodiscard]] bool node_tags_are_identical(std::vector<std::unique_ptr<Node>> const& nodes)
		{
			assert(!nodes.empty());
			return rs::all_of(++begin(nodes), end(nodes), [tag = nodes.front()->tag](std::unique_ptr<Node> const& node) noexcept { return node->tag == tag; });
		}

		bool longitude_is_in_range(LatLon longitude, std::pair<LatLon, LatLon> range) noexcept
		{
			// Longitude has a wraparound from top to bottom, so we need to check if the range covers the wraparound.
			if (range.first <= range.second) {
				// Range is [min, max].
				return range.first <= longitude && longitude <= range.second;
			}
			else {
				// Range is [max, ...] plus [..., min].
				return longitude <= range.first || range.second <= longitude;
			}
		}

		Area calculate_area_from_areas(std::vector<std::unique_ptr<Node>> const& nodes) noexcept
		{
			// Extract all areas to work on them.
			std::vector<Area> areas;
			areas.reserve(size(nodes) + 1);
			rs::transform(nodes, back_inserter(areas), [](std::unique_ptr<Node> const& node) noexcept { return get<DataArea>(node->data).area; });

			// Latitude is easy: just take min and max values
			Area result{ rs::min_element(areas, Coords::less_latitude, &Area::min)->min,
						 rs::max_element(areas, Coords::less_latitude, &Area::max)->max };

			// Longitude is more complicated, min might be greater than max. And areas might overlap
			// Sort by min longitude.
			rs::sort(areas, Coords::less_longitude, &Area::min);
			// Merge adjacent coords into one if they overlap
			for (auto iter{ begin(areas) }, next_iter{ iter + 1 }; next_iter != end(areas);) {
				// Next longitude range is fully covered by current longitude range, so just drop
				if (longitude_is_in_range(next_iter->max.longitude, std::pair{ iter->min.longitude, iter->max.longitude })) {
					next_iter = areas.erase(next_iter);
				}
				// Next longitude range overlaps with current longitude range, so take the new max and then drop it
				else if (longitude_is_in_range(next_iter->min.longitude, std::pair{ iter->min.longitude, iter->max.longitude })) {
					iter->max.longitude = next_iter->max.longitude;
					next_iter			= areas.erase(next_iter);
				}
				// No overlap
				else {
					iter = next_iter;
					++next_iter;
				}
			}
			// Append the first element at the end to simulate a wrap around.
			areas.push_back(areas.front());
			// Calculate distance for each original area to its next neighbor.
			std::vector<std::uint32_t> distances;
			distances.reserve(size(areas));
			rs::transform(
				areas | rv::take(size(nodes)), areas | rv::drop(1), back_inserter(distances),
				[](Coords const& lhs, Coords const& rhs) noexcept { return rhs.longitude - lhs.longitude; },
				&Area::min, &Area::max);
			// Find greatest distance
			auto const max_lon{ distance(begin(distances), rs::max_element(distances)) };
			result.max.longitude = areas.at(max_lon).max.longitude;
			result.min.longitude = areas.at(max_lon + 1).min.longitude;

			return result;
		}

		Area calculate_area_from_pois(std::vector<std::unique_ptr<Node>> const& nodes) noexcept
		{
			// Extract all coords to work on them.
			std::vector<Coords> coords;
			coords.reserve(size(nodes) + 1);
			rs::transform(nodes, back_inserter(coords), [](std::unique_ptr<Node> const& node) noexcept { return get<DataPoi>(node->data).coords; });

			// Latitude is easy: just take min and max values
			auto const [min_lat, max_lat]{ rs::minmax_element(coords, Coords::less_latitude) };
			Area result{ *min_lat, *max_lat };
			// Longitude is more complicated, min might be greater than max.
			// Sort by longitude.
			rs::sort(coords, Coords::less_longitude);
			// Append the first element at the end to simulate a wrap around.
			coords.push_back(coords.front());
			// Calculate distance for each original coord to its next neighbor.
			std::vector<std::uint32_t> distances;
			distances.reserve(size(coords));
			rs::transform(
				coords | rv::take(size(nodes)), coords | rv::drop(1), back_inserter(distances),
				[](LatLon const& lhs, LatLon const& rhs) noexcept { return rhs - lhs; },
				&Coords::longitude, &Coords::longitude);
			// Find greatest distance
			auto const max_lon{ distance(begin(distances), rs::max_element(distances)) };
			result.max.longitude = coords.at(max_lon).longitude;
			result.min.longitude = coords.at(max_lon + 1).longitude;

			return result;
		}

		Area calculate_area(std::vector<std::unique_ptr<Node>> const& nodes)
		{
			assert(!nodes.empty());
			switch (nodes.front()->tag) {
			case Tag::Area:
				return calculate_area_from_areas(nodes);
			case Tag::Poi:
				return calculate_area_from_pois(nodes);
			default:
				throw std::domain_error{ "Nodes must be Area or Poi" };
			}
		}
	}	// namespace

	std::string to_cp1252(std::wstring const& str)
	{
		assert(size(str));
		// WC_NO_BEST_FIT_CHARS | WC_COMPOSITECHECK
		auto len{ WideCharToMultiByte(1252, 0, data(str), gsl::narrow<int>(size(str)), nullptr, 0, nullptr, nullptr) };
		if (!len) {
			throw std::runtime_error{ "WideCharToMultiByte" };
		}
		std::string res;
		res.resize(len);
		len = WideCharToMultiByte(1252, 0, data(str), gsl::narrow<int>(size(str)), res.data(), gsl::narrow<int>(res.capacity()), nullptr, nullptr);
		if (!len) {
			throw std::runtime_error{ "WideCharToMultiByte" };
		}
		res.resize(len);

#if 1
		std::wstring wstr;
		wstr.resize(size(res));
		len = MultiByteToWideChar(1252, 0, data(res), gsl::narrow<int>(size(res)), wstr.data(), gsl::narrow<int>(wstr.capacity()));
		wstr.resize(len);
		assert(wstr == str);
#endif

		return res;
	}

	LatLon latitude(std::wstring const& str)
	{
		std::wistringstream is{ str };
		double degrees{};
		if (!(is >> degrees)
			|| degrees < -90
			|| degrees > 90) {
			throw std::invalid_argument{ "latitude" };
		}
		return LatLon{ degrees };
	}

	LatLon longitude(std::wstring const& str)
	{
		std::wistringstream is{ str };
		double degrees{};
		if (!(is >> degrees)
			|| degrees < -180
			|| degrees > 180) {
			throw std::invalid_argument{ "longitude" };
		}
		return LatLon{ degrees };
	}

	[[nodiscard]] std::pair<int, unsigned> LatLon::degrees_milliminutes() const
	{
		std::int64_t const val{ static_cast<std::int64_t>(value) * 360 };
		int const tenthmilliminutes{ gsl::narrow<int>(val * 60 * 10000 / 0x100000000LL) };
		int const milliminutes{ (tenthmilliminutes >= 0 ? (tenthmilliminutes + 5) : (tenthmilliminutes - 5)) / 10 };
		int const degrees{ milliminutes / 60000 };
		return std::make_pair(degrees, gsl::narrow<unsigned>(abs(milliminutes) % 60000));
	}

	std::ostream& operator<<(std::ostream& os, Coords const& coords)
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "{:dm}", coords);
		return os;
	}

	Area::Area(Coords const& min_, Coords const& max_) noexcept
		: min{ min_ }
		, max{ max_ }
	{
		assert(min.latitude <= max.latitude);
	}

	std::ostream& operator<<(std::ostream& os, Area const& area)
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "{}", area);
		return os;
	}

	bool Area::covers(Area const& other) noexcept
	{
		return covers(other.min)
			   && covers(other.max)
			   && (min.longitude <= max.longitude) == (other.min.longitude <= other.max.longitude);
	}

	bool Area::covers(Coords const& coords) noexcept
	{
		// Latitude is simple: it covers all values [min, max].
		if (!(min.latitude <= coords.latitude && coords.latitude <= max.latitude)) {
			return false;
		}
		return longitude_is_in_range(coords.longitude, std::pair{ min.longitude, max.longitude });
	}

	double Area::surface() const noexcept
	{
		constexpr double surface_globe{ 4 * nrs::pi };
		// Formula for Mantelfl�che from https://de.wikipedia.org/wiki/Kugelsegment
		double const surface_stripe{
			[this]() noexcept {
				auto const surface_segment{
					[](double latitude) noexcept {
						assert(0 <= latitude && latitude <= 90);
						double const alpha{ 2 * nrs::pi * (90 - latitude) / 360 };
						double const result{ 2 * nrs::pi * (1 - cos(alpha)) };
						return result;
					}
				};

				double const lat1{ min.latitude.degrees() };
				double const lat2{ max.latitude.degrees() };
				if (std::signbit(lat1) == std::signbit(lat2)) {
					return surface_segment(std::min(abs(lat1), abs(lat2))) - surface_segment(std::max(abs(lat1), abs(lat2)));
				}
				else {
					return surface_globe - surface_segment(abs(lat1)) - surface_segment(abs(lat2));
				}
			}()
		};

		double const surface_area{ surface_stripe * (max.longitude - min.longitude) / 0xffffffff };
		return surface_area / surface_globe;
	}

	std::size_t String::len() const noexcept
	{
		return sizeof(FileDataString) + size(str);
	}

	std::istream& String::read(std::istream& is, std::size_t maxlen)
	{
		std::size_t lenRead{};

		FileDataString filedata;
		lenRead += sizeof(filedata);
		if (lenRead > maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (!is.read(reinterpret_cast<char*>(&filedata), sizeof(filedata))) {
			return is;
		}
		if (lenRead + filedata.len > maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		str.resize(filedata.len);
		if (is.read(str.data(), size(str))) {
			return is;
		}
		assert(lenRead == len());
		return is;
	}

	std::ostream& String::write(std::ostream& os) const
	{
		FileDataString const filedata{ gsl::narrow<std::uint16_t>(size(str)) };
		os.write(reinterpret_cast<char const*>(&filedata), sizeof(filedata));
		os.write(str.data(), size(str));
		return os;
	}

	std::ostream& operator<<(std::ostream& os, String const& str)
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "{}", str);
		return os;
	}

	std::size_t LocalizedString::len() const noexcept
	{
		return sizeof(FileDataLocalizedString) + str.len();
	}

	std::istream& LocalizedString::read(std::istream& is, std::size_t maxlen)
	{
		std::size_t lenRead{};

		FileDataLocalizedString filedata;
		lenRead += sizeof(filedata);
		if (lenRead > maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (!is.read(reinterpret_cast<char*>(&filedata), sizeof(filedata))) {
			return is;
		}
		language = filedata.language;
		lenRead	 = sizeof(filedata.len) + filedata.len;
		if (lenRead > maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (!str.read(is, sizeof(filedata.len) + filedata.len)) {
			return is;
		}
		assert(lenRead == len());
		return is;
	}

	std::ostream& LocalizedString::write(std::ostream& os) const
	{
		FileDataLocalizedString const filedata{ language, str.len() };
		os.write(reinterpret_cast<char const*>(&filedata), sizeof(filedata));
		str.write(os);
		return os;
	}

	std::ostream& operator<<(std::ostream& os, LocalizedString const& str)
	{
#if 1
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "{}", str);
#endif
		return os;
	}

	StatisticsAreas StatisticsAreas::sum_counters(StatisticsAreas const& lhs, StatisticsAreas const& rhs) noexcept
	{
		return StatisticsAreas{
			lhs.count_areas_direct + rhs.count_areas_direct,
			lhs.count_areas_total + rhs.count_areas_total,
			lhs.count_pois_direct + rhs.count_pois_direct,
			lhs.count_pois_total + rhs.count_pois_total,
			std::max(lhs.max_depth, rhs.max_depth)
		};
	}

	DataStart::DataStart(std::chrono::system_clock::time_point time_, std::string name_, std::unique_ptr<Node> node_)
		: time{ std::move(time_) }
		, name{ name_ }
		, node{ std::move(node_) }
	{
		assert(!node || node->tag == Tag::StartExt);
	}

	std::size_t DataStart::len() const noexcept
	{
		return sizeof(FileDataStart) + name.len() + (node ? node->len() : 0);
	}

	std::optional<std::size_t> DataStart::len1() const noexcept
	{
		std::optional<std::size_t> result;
		if (node) {
			result = sizeof(FileDataStart) + name.len();
		}
		return result;
	}

	std::istream& DataStart::read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1)
	{
		std::size_t lenRead{};

		FileDataStart filedata;
		lenRead += sizeof(filedata);
		if (lenRead > maxlen1.value_or(maxlen)) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (!is.read(reinterpret_cast<char*>(&filedata), sizeof(filedata))) {
			return is;
		}
		if (filedata.marker != FileDataStart{}.marker) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (maxlen1.has_value()) {
			if (filedata.f1 != '1' || filedata.f2 != 3) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
		}
		else {
			if (filedata.f1 != '0' || filedata.f2 != 0) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
		}
		time = filedata.get_time();

		if (!name.read(is, maxlen1.value_or(maxlen) - lenRead)) {
			return is;
		}
		lenRead += name.len();
		if (lenRead != maxlen1.value_or(maxlen)) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		node.reset();
		if (maxlen1.has_value()) {
			node = std::make_unique<Node>();
			if (!node->read(is, maxlen - lenRead)) {
				return is;
			}
			lenRead += node->len();
			if (node->tag != Tag::StartExt) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
		}

		if (lenRead != maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataStart::write(std::ostream& os) const
	{
		FileDataStart const filedata{ !!node, time };
		os.write(reinterpret_cast<char const*>(&filedata), sizeof(filedata));
		name.write(os);
		if (node) {
			node->write(os);
		}
		return os;
	}

	std::ostream& DataStart::print(std::ostream& os, std::size_t indentation) const
	{
		// Check output time, compare with gpitool (_localtime_s) and with https://stackoverflow.com/a/46240575/1023911
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "start: {} {}\n"sv, floor<std::chrono::seconds>(time), name);
		if (node) {
			node->print(os, indentation + 2);
		}
		return os;
	}

	DataCodepage::DataCodepage(std::uint16_t codepage_, std::unique_ptr<Node> node_) noexcept
		: codepage{ codepage_ }
		, node{ std::move(node_) }
	{
		assert(!node || node->tag == Tag::License);
	}

	std::size_t DataCodepage::len() const noexcept
	{
		return sizeof(FileDataCodepage) + (node ? node->len() : 0);
	}

	std::optional<std::size_t> DataCodepage::len1() const noexcept
	{
		std::optional<std::size_t> result;
		if (node) {
			result = sizeof(FileDataCodepage);
		}
		return result;
	}

	std::istream& DataCodepage::read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1)
	{
		std::size_t lenRead{};

		FileDataCodepage filedata;
		lenRead += sizeof(filedata);
		if (lenRead > maxlen1.value_or(maxlen)) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (!is.read(reinterpret_cast<char*>(&filedata), sizeof(filedata))) {
			return is;
		}
		if (filedata.marker != FileDataCodepage{}.marker) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (maxlen1.has_value()) {
			if (filedata.f1 != '1' || filedata.subtag != Tag::License) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
		}
		else {
			if (filedata.f1 != '0' || filedata.subtag != Tag{}) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
		}
		codepage = filedata.codepage;
		if (lenRead != maxlen1.value_or(maxlen)) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		node.reset();
		if (maxlen1.has_value()) {
			node = std::make_unique<Node>();
			if (!node->read(is, maxlen - lenRead)) {
				return is;
			}
			lenRead += node->len();
			if (node->tag != filedata.subtag) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
		}

		if (lenRead != maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataCodepage::write(std::ostream& os) const
	{
		FileDataCodepage const filedata{ !!node, codepage };
		os.write(reinterpret_cast<char const*>(&filedata), sizeof(filedata));
		if (node) {
			node->write(os);
		}
		return os;
	}

	std::ostream& DataCodepage::print(std::ostream& os, std::size_t indentation) const
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "codepage: {}\n"sv, codepage);
		if (node) {
			node->print(os, indentation + 2);
		}
		return os;
	}

	std::size_t DataPoi::len() const noexcept
	{
		return sizeof(FileDataPoi)
			   + name.len()
			   + std::accumulate(begin(nodes), end(nodes), std::size_t{}, [](std::size_t sum, std::unique_ptr<Node> const& node) noexcept { return sum + node->len(); });
	}

	std::optional<std::size_t> DataPoi::len1() const noexcept
	{
		std::optional<std::size_t> result;
		if (!nodes.empty()) {
			result = sizeof(FileDataPoi) + name.len();
		}
		return result;
	}

	std::istream& DataPoi::read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1)
	{
		std::size_t lenRead{};

		FileDataPoi filedata;
		lenRead += sizeof(filedata);
		if (lenRead > maxlen1.value_or(maxlen)) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (!is.read(reinterpret_cast<char*>(&filedata), sizeof(filedata))) {
			return is;
		}
		if (LatLon{ filedata.latitude } < LatLon{ -90. } || LatLon{ filedata.latitude } > LatLon{ 90. }
			|| filedata.index != 1
			|| (filedata.flags & ~0x0300)) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		coords = Coords{ LatLon{ filedata.latitude }, LatLon{ filedata.longitude } };
		flags  = filedata.flags;
		if (!name.read(is, maxlen1.value_or(maxlen) - lenRead)) {
			return is;
		}
		lenRead += name.len();
		if (lenRead != maxlen1.value_or(maxlen)) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		nodes.clear();
		if (maxlen1.has_value()) {
			do {
				std::unique_ptr<Node> node{ std::make_unique<Node>() };
				if (!node->read(is, maxlen - lenRead)) {
					return is;
				}
				if (node->tag != Tag::ProximityAlarm
					&& node->tag != Tag::IndexBmp
					&& node->tag != Tag::IndexSubgroup
					&& node->tag != Tag::Cmt
					&& node->tag != Tag::Address
					&& node->tag != Tag::Contact
					&& node->tag != Tag::Desc) {
					is.setstate(std::ios::failbit | std::ios::badbit);
					return is;
				}
				lenRead += node->len();
				nodes.push_back(std::move(node));
			} while (lenRead != maxlen);
			if (!node_tags_are_unique(nodes)) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
		}

		[[gsl::suppress(type .1)]]	 // Don't use a static_cast for arithmetic conversions. Use brace initialization, gsl::narrow_cast or gsl::narrow (type.1).
		if (static_cast<bool>(flags & 0x0100) != has_proximity()) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		// For Garmin (downloaded) POI files we also see flags == 0x0200 (and DataDesc::garmin == true), but it is unclear what this value means.

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataPoi::write(std::ostream& os) const
	{
		FileDataPoi const filedata{ coords, flags };
		os.write(reinterpret_cast<char const*>(&filedata), sizeof(filedata));
		name.write(os);
		for (auto const& node : nodes) {
			node->write(os);
		}
		return os;
	}

	std::ostream& DataPoi::print(std::ostream& os, std::size_t indentation) const
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "poi: {} flags=0x{:04x} name={}\n"sv, coords, flags, name);
		for (auto const& node : nodes) {
			node->print(os, indentation + 2);
		}
		return os;
	}

	bool DataPoi::has_proximity() const noexcept
	{
		return rs::any_of(nodes, [](std::unique_ptr<Node> const& node) noexcept { return node->tag == Tag::ProximityAlarm; });
	}

	std::size_t DataProximityAlarm::len() const noexcept
	{
		return sizeof(FileDataProximityAlarm);
	}

	std::istream& DataProximityAlarm::read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1)
	{
		if (maxlen1.has_value()) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		std::size_t lenRead{};

		FileDataProximityAlarm filedata;
		lenRead += sizeof(filedata);
		if (lenRead > maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (!is.read(reinterpret_cast<char*>(&filedata), sizeof(filedata))) {
			return is;
		}
		if (filedata.marker1 != FileDataProximityAlarm{}.marker1
			|| (filedata.type != 0x10 && filedata.type != 0x20)
			|| (filedata.type == 0x10 && filedata.subtype != 4 && filedata.subtype != 5)
			|| (filedata.type == 0x10 && filedata.subtype == 4 && filedata.speed != 0)
			|| (filedata.type == 0x10 && filedata.subtype == 5 && filedata.speed == 0)) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		distance = filedata.distance;
		speed	 = filedata.speed;
		type	 = filedata.type;
		subtype	 = filedata.subtype;
		if (lenRead != maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataProximityAlarm::write(std::ostream& os) const
	{
		FileDataProximityAlarm const filedata{ type, subtype, distance, speed };
		os.write(reinterpret_cast<char const*>(&filedata), sizeof(filedata));
		return os;
	}

	std::ostream& DataProximityAlarm::print(std::ostream& os, std::size_t /*indentation*/) const
	{
		os << "proximity alarm: ";
		if (type == 0x10) {
			if (subtype == 4) {
				[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
				std::format_to(std::ostreambuf_iterator{ os }, "approaching distance={} m"sv, distance);
			}
			else {
				assert(subtype == 5);
				[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
				std::format_to(std::ostreambuf_iterator{ os }, "speed limit distance={} m speed={} 100 m/s"sv, distance, speed);
			}
		}
		else {
			assert(type == 0x20);
			[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
			std::format_to(std::ostreambuf_iterator{ os }, "speed limit index mp3={:d} distance={} m speed={} 100 m/s"sv, subtype, distance, speed);
		}
		os << '\n';
		return os;
	}

	std::size_t DataIndexBmp::len() const noexcept
	{
		return sizeof(index);
	}

	std::istream& DataIndexBmp::read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1)
	{
		if (maxlen1.has_value()) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		if (sizeof(index) != maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		is.read(reinterpret_cast<char*>(&index), sizeof(index));

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataIndexBmp::write(std::ostream& os) const
	{
		os.write(reinterpret_cast<char const*>(&index), sizeof(index));
		return os;
	}

	std::ostream& DataIndexBmp::print(std::ostream& os, std::size_t /*indentation*/) const
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "index bmp: {}\n"sv, index);
		return os;
	}

	std::size_t DataBmp::len() const noexcept
	{
		return sizeof(FileDataBmp) + size(pixels) + size(palette) * sizeof(Color) + paddingBytes;
	}

	std::istream& DataBmp::read(std::istream& is, std::size_t maxlen, [[maybe_unused]] std::optional<std::size_t> maxlen1)
	{
		if (maxlen1.has_value()) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		FileDataBmp filedata;
		std::size_t lenRead{ sizeof(filedata) };
		if (lenRead > maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (!is.read(reinterpret_cast<char*>(&filedata), sizeof(filedata))) {
			return is;
		}
		if (!filedata.height
			|| !filedata.width
			|| filedata.stride != calcStride(filedata.width, filedata.bitcount)
			|| (filedata.bitcount != 1 && filedata.bitcount != 4 && filedata.bitcount != 8 && filedata.bitcount != 24 && filedata.bitcount != 32)
			|| filedata.compression
			|| filedata.sizeImage != gsl::narrow<std::uint32_t>(filedata.stride * filedata.height)
			|| filedata.offsetImage != Node::hdrLen(maxlen1.has_value()) + sizeof(filedata)
			|| filedata.maxPaletteSize != paletteSize(filedata.bitcount)
			|| filedata.marker != FileDataBmp{}.marker
			|| (filedata.unknown != 0 && filedata.unknown != 1)
			|| filedata.offsetPalette != (isPaletteBased(filedata.bitcount) ? Node::hdrLen(maxlen1.has_value()) + sizeof(filedata) + filedata.sizeImage : 0)) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		index	 = filedata.index;
		width	 = filedata.width;
		height	 = filedata.height;
		bitcount = filedata.bitcount;
		unknown	 = filedata.unknown;

		lenRead += filedata.sizeImage;
		if (lenRead > maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		pixels.resize(filedata.sizeImage);
		is.read(pixels.data(), size(pixels));

		if (isPaletteBased(filedata.bitcount)) {
			std::size_t const entriesPalette{ (maxlen - lenRead) / sizeof(Color) };
			lenRead += entriesPalette * sizeof(Color);
			if (entriesPalette > filedata.maxPaletteSize
				|| lenRead > maxlen) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
			palette.resize(entriesPalette);
			[[gsl::suppress(io .2)]]   // Arithmetic overflow: Using operator '*' on a 4 byte value and then casting the result to a 8 byte value. Cast the value to the wider type before calling operator '*' to avoid overflow (io.2).
			is.read(reinterpret_cast<char*>(palette.data()), size(palette) * sizeof(Color));
		}
		paddingBytes = 0;
		while (lenRead != maxlen) {
			char c;
			is >> c;
			if (c) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
			++paddingBytes;
			++lenRead;
		}
		assert(lenRead == maxlen);

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataBmp::write(std::ostream& os) const
	{
		FileDataBmp const filedata{ Node::hdrLen(len1().has_value()), index, width, height, bitcount, unknown };
		os.write(reinterpret_cast<char const*>(&filedata), sizeof(filedata));
		assert(filedata.sizeImage == size(pixels));
		os.write(pixels.data(), size(pixels));
		if (isPaletteBased(bitcount)) {
			assert(size(palette) <= paletteSize(filedata.bitcount));
			[[gsl::suppress(io .2)]]   // Arithmetic overflow: Using operator '*' on a 4 byte value and then casting the result to a 8 byte value. Cast the value to the wider type before calling operator '*' to avoid overflow (io.2).
			os.write(reinterpret_cast<char const*>(palette.data()), size(palette) * sizeof(Color));
		}
		else {
			assert(palette.empty());
		}
		for (std::size_t i{}; i < paddingBytes; ++i) {
			os << char{};
		}
		return os;
	}

	std::ostream& DataBmp::print(std::ostream& os, std::size_t /*indentation*/) const
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "bmp: index={} pixels={}x{} bitcount={} unknown={} paletteEntries={} paddingBytes={}\n"sv,
					   index, width, height, bitcount, unknown, size(palette), paddingBytes);
		return os;
	}

	std::size_t DataIndexSubgroup::len() const noexcept
	{
		return sizeof(index);
	}

	std::istream& DataIndexSubgroup::read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1)
	{
		if (maxlen1.has_value()) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		if (sizeof(index) != maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		is.read(reinterpret_cast<char*>(&index), sizeof(index));

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataIndexSubgroup::write(std::ostream& os) const
	{
		os.write(reinterpret_cast<char const*>(&index), sizeof(index));
		return os;
	}

	std::ostream& DataIndexSubgroup::print(std::ostream& os, std::size_t /*indentation*/) const
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "index subgroup: {}\n"sv, index);
		return os;
	}

	DataSubgroup::DataSubgroup(std::uint16_t index_, LocalizedString name_, std::unique_ptr<Node> node_)
		: index{ index_ }
		, name{ name_ }
		, node{ std::move(node_) }
	{
		assert(!node || node->tag == Tag::IndexBmp);
	}

	std::size_t DataSubgroup::len() const noexcept
	{
		return sizeof(index)
			   + name.len()
			   + (node ? node->len() : std::size_t{});
	}

	std::optional<std::size_t> DataSubgroup::len1() const noexcept
	{
		std::optional<std::size_t> result;
		if (!!node) {
			result = sizeof(index) + name.len();
		}
		return result;
	}

	std::istream& DataSubgroup::read(std::istream& is, std::size_t maxlen, [[maybe_unused]] std::optional<std::size_t> maxlen1)
	{
		std::size_t lenRead{ sizeof(index) };
		if (lenRead > maxlen1.value_or(maxlen)) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (!is.read(reinterpret_cast<char*>(&index), sizeof(index))) {
			return is;
		}
		name.read(is, maxlen1.value_or(maxlen) - lenRead);
		lenRead += name.len();
		if (lenRead != maxlen1.value_or(maxlen)) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		node.reset();
		if (maxlen1.has_value()) {
			node = std::make_unique<Node>();
			if (!node->read(is, maxlen - lenRead)) {
				return is;
			}
			if (node->tag != Tag::IndexBmp) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
			lenRead += node->len();
		}

		if (lenRead != maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataSubgroup::write(std::ostream& os) const
	{
		os.write(reinterpret_cast<char const*>(&index), sizeof(index));
		name.write(os);
		if (!!node) {
			node->write(os);
		}
		return os;
	}

	std::ostream& DataSubgroup::print(std::ostream& os, std::size_t indentation) const
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "subgroup: index={} {}\n"sv, index, name);
		if (!!node) {
			node->print(os, indentation + 2);
		}
		return os;
	}

	std::size_t DataArea::len() const noexcept
	{
		return sizeof(FileDataArea)
			   + std::accumulate(begin(nodes), end(nodes), std::size_t{}, [](std::size_t sum, std::unique_ptr<Node> const& node) noexcept { return sum + node->len(); });
	}

	std::optional<std::size_t> DataArea::len1() const noexcept
	{
		std::optional<std::size_t> result;
		assert(!nodes.empty());
		result = sizeof(FileDataArea);
		return result;
	}

	std::istream& DataArea::read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1)
	{
		if (!maxlen1.has_value()) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		std::size_t lenRead{};

		FileDataArea filedata;
		lenRead += sizeof(filedata);
		if (lenRead > maxlen1.value()) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (!is.read(reinterpret_cast<char*>(&filedata), sizeof(filedata))) {
			return is;
		}
		if (filedata.marker != FileDataArea{}.marker
			|| (filedata.flags & ~0x0300)
			|| filedata.minLatitude > filedata.maxLatitude) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		area  = Area{ Coords{ LatLon{ filedata.minLatitude }, LatLon{ filedata.minLongitude } },
					  Coords{ LatLon{ filedata.maxLatitude }, LatLon{ filedata.maxLongitude } } };
		flags = filedata.flags;
		if (lenRead != maxlen1.value()) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		nodes.clear();
		do {
			std::unique_ptr<Node> node{ std::make_unique<Node>() };
			if (!node->read(is, maxlen - lenRead)) {
				return is;
			}
			if (node->tag != Tag::Area
				&& node->tag != Tag::Poi) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
			lenRead += node->len();
			nodes.push_back(std::move(node));
		} while (lenRead != maxlen);
		if (!node_tags_are_identical(nodes)) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		auto const areInArea{ [this](std::unique_ptr<Node> const& node) noexcept {
			switch (node->tag) {
			case Tag::Poi:
				return area.covers(std::get<DataPoi>(node->data).coords);
			case Tag::Area:
				return area.covers(std::get<DataArea>(node->data).area);
			default:
				assert(false);
				return false;
			}
		} };
		if (!rs::all_of(nodes, areInArea)) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		[[gsl::suppress(type .1)]]	 // Don't use a static_cast for arithmetic conversions. Use brace initialization, gsl::narrow_cast or gsl::narrow (type.1).
		if (static_cast<bool>(filedata.flags & Flag_has_proximity) != has_proximity()) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataArea::write(std::ostream& os) const
	{
		FileDataArea const filedata{ area, flags };
		os.write(reinterpret_cast<char const*>(&filedata), sizeof(filedata));
		for (auto const& node : nodes) {
			node->write(os);
		}
		return os;
	}

	std::ostream& DataArea::print(std::ostream& os, std::size_t indentation) const
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "area: {} flags=0x{:04x}", area, flags);
		if (statistics.has_value()) {
			[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
			std::format_to(std::ostreambuf_iterator{ os }, " areas={}/{} pois={}/{} max_depth={} surface={:f}"sv,
						   statistics->count_areas_direct, statistics->count_areas_total,
						   statistics->count_pois_direct, statistics->count_pois_total,
						   statistics->max_depth,
						   area.surface() * 1000);
			if (statistics->percent_surface.has_value()) {
				[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
				std::format_to(std::ostreambuf_iterator{ os }, " ({:.1f} %)"sv, statistics->percent_surface.value());
			}
			if (statistics->min_area != area) {
				[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
				std::format_to(std::ostreambuf_iterator{ os }, " min_area={} ({:.1f} %)"sv, statistics->min_area, statistics->min_area.surface() / area.surface() * 100);
			}
		}
		os << '\n';
		for (auto const& node : nodes) {
			node->print(os, indentation + 2);
		}
		return os;
	}

	void DataArea::update_statistics()
	{
		assert(!nodes.empty());
		switch (nodes.front()->tag) {
		case Tag::Area: {
			// First calculate statistics for each sub area.
			rs::for_each(nodes, [](std::unique_ptr<Node> const& node) { return get<DataArea>(node->data).update_statistics(); });
			// Then calculate our statistics from the calculated statistics.
			auto const get_statistics_from_node{
				[](std::unique_ptr<Node> const& node) { return get<DataArea>(node->data).statistics.value(); }
			};
			statistics = std::transform_reduce(begin(nodes), end(nodes), StatisticsAreas{}, StatisticsAreas::sum_counters, get_statistics_from_node);

			statistics->count_areas_direct = size(nodes);
			statistics->count_areas_total += size(nodes);
			statistics->count_pois_direct = 0;
			++statistics->max_depth;
			statistics->min_area = calculate_area_from_areas(nodes);

			if (auto const surface{ area.surface() }; surface) {
				rs::for_each(nodes, [surface](std::unique_ptr<Node> const& node) {
					auto& data_area{ std::get<DataArea>(node->data) };
					data_area.statistics->percent_surface = data_area.area.surface() / surface * 100;
				});
			}
			break;
		}
		case Tag::Poi:
			statistics.emplace(0, 0, size(nodes), size(nodes), 0, calculate_area_from_pois(nodes));
			break;
		default:
			throw std::domain_error{ "Nodes must be Area or Poi" };
		}
	}

	void DataArea::recalculate_area()
	{
		update_statistics();
		recalculate_area_impl();
	}

	void DataArea::recalculate_flags()
	{
		if (has_proximity()) {
			flags |= DataArea::Flag_has_proximity;
		}
		else {
			flags &= ~DataArea::Flag_has_proximity;
		}
	}

	bool DataArea::has_proximity() const
	{
		return rs::any_of(nodes, [](std::unique_ptr<Node> const& node) {
			switch (node->tag) {
			case Tag::Area:
				return std::get<DataArea>(node->data).has_proximity();
			case Tag::Poi:
				return std::get<DataPoi>(node->data).has_proximity();
			default:
				throw std::domain_error{ "Nodes must be Area or Poi" };
			}
		});
	}

	void DataArea::recalculate_area_impl()
	{
		assert(!nodes.empty());
		if (nodes.front()->tag == Tag::Area) {
			// First calculate statistics for each sub area.
			rs::for_each(nodes, [](std::unique_ptr<Node> const& node) { return get<DataArea>(node->data).recalculate_area(); });
		}
		area = statistics->min_area;
	}

	std::size_t DataGroup::len() const noexcept
	{
		return name.len()
			   + area->len()
			   + std::accumulate(begin(metadata), end(metadata), std::size_t{}, [](std::size_t sum, std::unique_ptr<Node> const& node) noexcept { return sum + node->len(); });
	}

	std::optional<std::size_t> DataGroup::len1() const noexcept
	{
		std::optional<std::size_t> result;
		if (!metadata.empty()) {
			result = name.len() + area->len();
		}
		return result;
	}

	std::istream& DataGroup::read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1)
	{
		std::size_t lenRead{};

		if (!name.read(is, maxlen1.value_or(maxlen))) {
			return is;
		}
		lenRead += name.len();

		area = std::make_unique<Node>();
		if (!area->read(is, maxlen1.value_or(maxlen) - lenRead)) {
			return is;
		}
		if (area->tag != Tag::Area) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		lenRead += area->len();
		if (lenRead != maxlen1.value_or(maxlen)) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		metadata.clear();
		if (maxlen1.has_value()) {
			do {
				std::unique_ptr<Node> node{ std::make_unique<Node>() };
				if (!node->read(is, maxlen - lenRead)) {
					return is;
				}
				if (node->tag != Tag::Bmp
					&& node->tag != Tag::Subgroup
					&& node->tag != Tag::Mp3) {
					is.setstate(std::ios::failbit | std::ios::badbit);
					return is;
				}
				lenRead += node->len();
				metadata.push_back(std::move(node));
			} while (lenRead != maxlen);
		}

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataGroup::write(std::ostream& os) const
	{
		name.write(os);
		area->write(os);
		for (auto const& node : metadata) {
			node->write(os);
		}
		return os;
	}

	std::ostream& DataGroup::print(std::ostream& os, std::size_t indentation) const
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "group: {}\n", name);
		area->print(os, indentation + 2);
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "{:{}}metadata:\n"sv, "", indentation + 2);
		for (auto const& node : metadata) {
			node->print(os, indentation + 2);
		}
		return os;
	}

	std::size_t DataCmt::len() const noexcept
	{
		return cmtdesc.len();
	}

	std::istream& DataCmt::read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1)
	{
		if (maxlen1.has_value()) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		std::size_t lenRead{};

		cmtdesc.read(is, maxlen - lenRead);
		lenRead += cmtdesc.len();
		if (lenRead != maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataCmt::write(std::ostream& os) const
	{
		cmtdesc.write(os);
		return os;
	}

	std::ostream& DataCmt::print(std::ostream& os, std::size_t /*indentation*/) const
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "cmt: {}\n", cmtdesc);
		return os;
	}

	std::size_t DataAddress::len() const noexcept
	{
		return sizeof(FileDataAddress)
			   + (city.empty() ? 0 : city.len())
			   + (country.empty() ? 0 : country.len())
			   + (state.empty() ? 0 : state.len())
			   + (postcode.empty() ? 0 : postcode.len())
			   + (street.empty() ? 0 : street.len())
			   + (housenr.empty() ? 0 : housenr.len());
	}

	std::istream& DataAddress::read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1)
	{
		_len1 = maxlen1;

		std::size_t lenRead{};

		FileDataAddress filedata;
		if (maxlen1.has_value() && maxlen1.value() != sizeof(filedata)) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		lenRead += sizeof(filedata);
		if (lenRead > maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (!is.read(reinterpret_cast<char*>(&filedata), sizeof(filedata))) {
			return is;
		}
		if (filedata.flags & ~0x003F) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (filedata.flags & 1) {
			city.read(is, maxlen - lenRead);
			assert(!city.empty());	 // In this case our write would not be correct.
			lenRead += city.len();
		}
		if (is && filedata.flags & 2) {
			country.read(is, maxlen - lenRead);
			assert(!country.empty());	// In this case our write would not be correct.
			lenRead += country.len();
		}
		if (is && filedata.flags & 4) {
			state.read(is, maxlen - lenRead);
			assert(!state.empty());	  // In this case our write would not be correct.
			lenRead += state.len();
		}
		if (is && filedata.flags & 8) {
			postcode.read(is, maxlen - lenRead);
			assert(!postcode.empty());	 // In this case our write would not be correct.
			lenRead += postcode.len();
		}
		if (is && filedata.flags & 0x10) {
			street.read(is, maxlen - lenRead);
			assert(!street.empty());   // In this case our write would not be correct.
			lenRead += street.len();
		}
		if (is && filedata.flags & 0x20) {
			housenr.read(is, maxlen - lenRead);
			assert(!housenr.empty());	// In this case our write would not be correct.
			lenRead += housenr.len();
		}
		if (lenRead != maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataAddress::write(std::ostream& os) const
	{
		FileDataAddress const filedata{ !city.empty(), !country.empty(), !state.empty(), !postcode.empty(), !street.empty(), !housenr.empty() };
		os.write(reinterpret_cast<char const*>(&filedata), sizeof(filedata));
		if (!city.empty()) {
			city.write(os);
		}
		if (!country.empty()) {
			country.write(os);
		}
		if (!state.empty()) {
			state.write(os);
		}
		if (!postcode.empty()) {
			postcode.write(os);
		}
		if (!street.empty()) {
			street.write(os);
		}
		if (!housenr.empty()) {
			housenr.write(os);
		}
		return os;
	}

	std::ostream& DataAddress::print(std::ostream& os, std::size_t /*indentation*/) const
	{
		os << "address:";
		if (!city.empty()) {
			os << " city=" << city;
		}
		if (!country.empty()) {
			os << " country=" << country;
		}
		if (!state.empty()) {
			os << " state=" << state;
		}
		if (!postcode.empty()) {
			os << " postcode=" << postcode;
		}
		if (!street.empty()) {
			os << " street=" << street;
		}
		if (!housenr.empty()) {
			os << " housenr=" << housenr;
		}
		if (auto const length1{ len1() }; length1.has_value()) {
			[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
			std::format_to(std::ostreambuf_iterator{ os }, ", len1={}"sv, length1.value());
		}
		os << '\n';
		return os;
	}

	std::size_t DataContact::len() const noexcept
	{
		return sizeof(FileDataContact)
			   + (phone.empty() ? 0 : phone.len())
			   + (mail.empty() ? 0 : mail.len())
			   + (web.empty() ? 0 : web.len());
	}

	std::istream& DataContact::read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1)
	{
		_len1 = maxlen1;

		std::size_t lenRead{};

		FileDataContact filedata;
		lenRead += sizeof(filedata);
		if (lenRead > maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (!is.read(reinterpret_cast<char*>(&filedata), sizeof(filedata))) {
			return is;
		}
		if (filedata.flags & ~0x0019) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (filedata.flags & 1) {
			phone.read(is, maxlen - lenRead);
			assert(!phone.empty());	  // In this case our write would not be correct.
			lenRead += phone.len();
		}
		if (is && filedata.flags & 8) {
			mail.read(is, maxlen - lenRead);
			assert(!mail.empty());	 // In this case our write would not be correct.
			lenRead += mail.len();
		}
		if (is && filedata.flags & 0x10) {
			web.read(is, maxlen - lenRead);
			assert(!web.empty());	// In this case our write would not be correct.
			lenRead += web.len();
		}
		if (lenRead != maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataContact::write(std::ostream& os) const
	{
		FileDataContact const filedata{ !phone.empty(), !mail.empty(), !web.empty() };
		os.write(reinterpret_cast<char const*>(&filedata), sizeof(filedata));
		if (!phone.empty()) {
			phone.write(os);
		}
		if (!mail.empty()) {
			mail.write(os);
		}
		if (!web.empty()) {
			web.write(os);
		}
		return os;
	}

	std::ostream& DataContact::print(std::ostream& os, std::size_t /*indentation*/) const
	{
		os << "contact:";
		if (!phone.empty()) {
			os << " tel=" << phone;
		}
		if (!mail.empty()) {
			os << " mail=" << mail;
		}
		if (!web.empty()) {
			os << " web=" << web;
		}
		os << '\n';
		return os;
	}

	std::size_t DataDesc::len() const noexcept
	{
		return sizeof(FileDataDesc) + (desc.empty() ? 0 : desc.len());
	}

	std::istream& DataDesc::read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1)
	{
		if (maxlen1.has_value()) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		std::size_t lenRead{};

		FileDataDesc filedata;
		lenRead += sizeof(filedata);
		if (lenRead > maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (!is.read(reinterpret_cast<char*>(&filedata), sizeof(filedata))) {
			return is;
		}
		if (filedata.flags & ~0x0005) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		garmin = filedata.flags & 4;
		if (filedata.flags & 1) {
			desc.read(is, maxlen - lenRead);
			assert(!desc.empty());	 // In this case our write would not be correct.
			lenRead += desc.len();
		}
		if (lenRead != maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataDesc::write(std::ostream& os) const
	{
		FileDataDesc const filedata{ garmin, !desc.empty() };
		os.write(reinterpret_cast<char const*>(&filedata), sizeof(filedata));
		if (!desc.empty()) {
			desc.write(os);
		}
		return os;
	}

	std::ostream& DataDesc::print(std::ostream& os, std::size_t /*indentation*/) const
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "desc: garmin={:s}", garmin);
		if (!desc.empty()) {
			os << " desc=" << desc;
		}
		os << '\n';
		return os;
	}

	std::size_t DataStartExt::len() const noexcept
	{
		return size(data);
	}

	std::istream& DataStartExt::read(std::istream& is, [[maybe_unused]] std::size_t maxlen, [[maybe_unused]] std::optional<std::size_t> maxlen1)
	{
		if (maxlen1.has_value()) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		is.read(reinterpret_cast<char*>(data.data()), size(data));
		assert(data.at(3) == 0);
		assert(data.at(4) == 0);

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataStartExt::write(std::ostream& os) const
	{
		os.write(reinterpret_cast<char const*>(data.data()), size(data));
		return os;
	}

	std::ostream& DataStartExt::print(std::ostream& os, std::size_t /*indentation*/) const
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "startext: 0x{:02x}:{:02x}:{:02x}:{:02x}:{:02x}\n"sv, data.at(0), data.at(1), data.at(2), data.at(3), data.at(4));
		return os;
	}

	std::size_t DataLicense::len() const noexcept
	{
		return sizeof(FileDataLicense) + name.len() + license.len();
	}

	std::istream& DataLicense::read(std::istream& is, std::size_t maxlen, [[maybe_unused]] std::optional<std::size_t> maxlen1)
	{
		if (maxlen1.has_value()) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		std::size_t lenRead{};

		FileDataLicense filedata;
		lenRead += sizeof(filedata);
		if (lenRead > maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		if (!is.read(reinterpret_cast<char*>(&filedata), sizeof(filedata))) {
			return is;
		}
		if (filedata.marker != FileDataLicense{}.marker) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		name.read(is, maxlen - lenRead);
		assert(!name.empty());	 // In this case our write would not be correct.
		lenRead += name.len();
		license.read(is, maxlen - lenRead);
		assert(!license.empty());	// In this case our write would not be correct.
		lenRead += license.len();
		if (lenRead != maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataLicense::write(std::ostream& os) const
	{
		FileDataLicense const filedata{};
		os.write(reinterpret_cast<char const*>(&filedata), sizeof(filedata));
		if (!name.empty()) {
			name.write(os);
		}
		if (!license.empty()) {
			license.write(os);
		}
		return os;
	}

	std::ostream& DataLicense::print(std::ostream& os, std::size_t /*indentation*/) const
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "license: name={} license={}\n", name, license);
		return os;
	}

	std::size_t DataMp3::len() const noexcept
	{
		return sizeof(FileDataMp3_1)
			   + sizeof(FileDataMp3_2)
			   + size(mp3);
	}

	std::optional<std::size_t> DataMp3::len1() const noexcept
	{
		return { sizeof(FileDataMp3_1) };
	}

	std::istream& DataMp3::read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1)
	{
		if (!maxlen1.has_value()) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		std::size_t lenRead{};

		{
			FileDataMp3_1 filedata;
			lenRead += sizeof(filedata);
			if (lenRead > maxlen1.value()) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
			if (!is.read(reinterpret_cast<char*>(&filedata), sizeof(filedata))) {
				return is;
			}
			if (filedata.marker != FileDataMp3_1{}.marker) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
			index = filedata.index;
			if (lenRead != maxlen1.value()) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
		}

		{
			FileDataMp3_2 filedata;
			lenRead += sizeof(filedata);
			if (lenRead > maxlen) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
			if (!is.read(reinterpret_cast<char*>(&filedata), sizeof(filedata))) {
				return is;
			}
			if (filedata.len != sizeof(filedata) + filedata.lenMp3
				|| filedata.lenMp3 != maxlen - lenRead) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
			language = filedata.language;
			mp3.resize(filedata.lenMp3);
			is.read(mp3.data(), size(mp3));
			lenRead += size(mp3);

			assert(lenRead == maxlen);
		}

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataMp3::write(std::ostream& os) const
	{
		{
			FileDataMp3_1 const filedata{ index };
			os.write(reinterpret_cast<char const*>(&filedata), sizeof(filedata));
		}
		{
			FileDataMp3_2 const filedata{ language, size(mp3) };
			os.write(reinterpret_cast<char const*>(&filedata), sizeof(filedata));
		}
		os.write(mp3.data(), size(mp3));
		return os;
	}

	std::ostream& DataMp3::print(std::ostream& os, std::size_t /*indentation*/) const
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "mp3: index={:d} {}{} {} bytes\n"sv, index, language.at(0), language.at(1), size(mp3));
		return os;
	}

	std::istream& DataEof::read(std::istream& is, [[maybe_unused]] std::size_t maxlen, [[maybe_unused]] std::optional<std::size_t> maxlen1)
	{
		if (maxlen || maxlen1.has_value()) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataEof::print(std::ostream& os, std::size_t /*indentation*/) const
	{
		return os << "eof\n";
	}

	std::size_t DataOther::len() const noexcept
	{
		return size(data);
	}

	std::istream& DataOther::read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1)
	{
		_len1 = maxlen1;
		data.resize(maxlen);
		is.read(data.data(), size(data));

		assert(len1() == maxlen1);
		assert(len() == maxlen);
		return is;
	}

	std::ostream& DataOther::write(std::ostream& os) const
	{
		os.write(data.data(), size(data));
		return os;
	}

	std::ostream& DataOther::print(std::ostream& os, std::size_t /*indentation*/) const
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "other: {} bytes"sv, len());
		if (auto const length1{ len1() }; length1.has_value()) {
			[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
			std::format_to(std::ostreambuf_iterator{ os }, ", len1={}"sv, length1.value());
		}
		os << '\n';
		return os;
	}

	std::size_t Node::hdrLen() const noexcept
	{
		return hdrLen(std::visit([](auto&& arg) noexcept { return arg.len1().has_value(); }, data));
	}

	[[gsl::suppress(f .4)]]	  // You can attempt to make 'gpilib::Node::hdrLen' constexpr unless it contains any undefined behavior
	std::size_t
	Node::hdrLen(bool withLen1) noexcept
	{
		return sizeof(FileDataNode) + (withLen1 ? sizeof(std::uint32_t) : std::size_t{});
	}

	std::size_t Node::len() const noexcept
	{
		return hdrLen() + std::visit([](auto&& arg) noexcept { return arg.len(); }, data);
	}

	std::istream& Node::read(std::istream& is, std::size_t maxlen)
	{
		std::size_t lenRead{};

		FileDataNode filedata{};
		lenRead += sizeof(FileDataNode);
		if (lenRead > maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		if (!is.read(reinterpret_cast<char*>(&filedata), sizeof(filedata))) {
			return is;
		}
		tag = filedata.tag;
		if (filedata.flags != 0 && filedata.flags != 8) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		std::optional<std::size_t> len1;
		if (filedata.flags == 8) {
			std::uint32_t len1_{};
			lenRead += sizeof(len1_);
			if (lenRead > maxlen) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
			is.read(reinterpret_cast<char*>(&len1_), sizeof(len1_));
			if (!is) {
				return is;
			}
			if (len1_ > filedata.len) {
				is.setstate(std::ios::failbit | std::ios::badbit);
				return is;
			}
			len1 = len1_;
		}
		if (lenRead + filedata.len > maxlen) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}

		switch (tag) {
		case Tag::Start:
			data = DataStart{};
			break;
		case Tag::Codepage:
			data = DataCodepage{};
			break;
		case Tag::Poi:
			data = DataPoi{};
			break;
		case Tag::ProximityAlarm:
			data = DataProximityAlarm{};
			break;
		case Tag::IndexBmp:
			data = DataIndexBmp{};
			break;
		case Tag::Bmp:
			data = DataBmp{};
			break;
		case Tag::IndexSubgroup:
			data = DataIndexSubgroup{};
			break;
		case Tag::Subgroup:
			data = DataSubgroup{};
			break;
		case Tag::Area:
			data = DataArea{};
			break;
		case Tag::Group:
			data = DataGroup{};
			break;
		case Tag::Cmt:
			data = DataCmt{};
			break;
		case Tag::Address:
			data = DataAddress{};
			break;
		case Tag::Contact:
			data = DataContact{};
			break;
		// 000D: !len1. Image (Exif, Adobe Photoshop)? Currently no sample gpi index available
		case Tag::Desc:
			data = DataDesc{};
			break;
		case Tag::StartExt:
			data = DataStartExt{};
			break;
		case Tag::License:
			data = DataLicense{};
			break;
		case Tag::Mp3:
			data = DataMp3{};
			break;
		case Tag::Eof:
			data = DataEof{};
			break;
		default:
			assert(false);	 // Not a problem for us but a trigger to add new unknown tags
			data = DataOther{};
			break;
		}
		std::visit([&is, len = filedata.len, len1](auto&& arg) { arg.read(is, len, len1); }, data);

		return is;
	}

	std::ostream& Node::write(std::ostream& os) const
	{
		std::size_t const len_data{ std::visit([](auto&& arg) noexcept { return arg.len(); }, data) };
		std::optional<std::size_t> const len1{ std::visit([](auto&& arg) noexcept { return arg.len1(); }, data) };
		FileDataNode const filedata{ tag, len1.has_value() ? std::uint16_t{ 8 } : std::uint16_t{}, gsl::narrow<std::uint32_t>(len_data) };
		os.write(reinterpret_cast<char const*>(&filedata), sizeof(filedata));
		if (len1.has_value()) {
			std::uint32_t const length{ gsl::narrow<std::uint32_t>(len1.value()) };
			os.write(reinterpret_cast<char const*>(&length), sizeof(length));
		}
		std::visit([&os, this](auto&& arg) noexcept(noexcept(arg.write(os))) { arg.write(os); }, data);
		return os;
	}

	std::ostream& Node::print(std::ostream& os, std::size_t indentation) const
	{
		[[gsl::suppress(bounds .1)]]	  // warning C26481: Don't use pointer arithmetic. Use span instead (bounds.1).
		std::format_to(std::ostreambuf_iterator{ os }, "{:{}}{:04x} "sv, "", indentation, static_cast<std::underlying_type_t<decltype(tag)>>(tag));
		std::visit([&os, indentation](auto&& arg) { arg.print(os, indentation); }, data);
		return os;
	}

	std::istream& Gpi::read(std::istream& is)
	{
		nodes.clear();
		noskipws(is);
		while (is.peek() != std::istream::traits_type::eof()) {
			Node node;
			if (!node.read(is, std::numeric_limits<std::size_t>::max())) {
				return is;
			}
			nodes.push_back(std::move(node));
		}
		// First node must be Start, second Codepage, then a non empty list of Group and at the end Eof.
		if (size(nodes) < 4
			|| nodes.at(0).tag != Tag::Start
			|| nodes.at(1).tag != Tag::Codepage
			|| nodes.back().tag != Tag::Eof
			|| std::any_of(begin(nodes) + 2, --end(nodes), [](Node const& node) noexcept { return node.tag != Tag::Group; })) {
			is.setstate(std::ios::failbit | std::ios::badbit);
			return is;
		}
		return is;
	}

	std::ostream& Gpi::write(std::ostream& os) const
	{
		for (auto const& node : nodes) {
			node.write(os);
		}
		return os;
	}

	std::ostream& operator<<(std::ostream& os, Gpi const& gpi)
	{
		rs::copy(gpi.nodes, std::ostream_iterator<Node>{ os });
		return os;
	}

	void AreaSplittingNTree::split(Node& node_area)
	{
		assert(target_pois_per_area <= max_pois_per_area);
		assert(default_subareas <= max_subareas);

		DataArea& data_area{ std::get<DataArea>(node_area.data) };
		assert(!data_area.nodes.empty());
		assert(data_area.nodes.front()->tag == Tag::Poi);

		if (size(data_area.nodes) <= max_pois_per_area) {
			return;
		}

		auto const coords_from_poi_node{
			[](std::unique_ptr<Node> const& node) noexcept { return std::get<DataPoi>(node->data).coords; }
		};
		// Split by latitude or by longitude?
		if (data_area.area.max.latitude - data_area.area.min.latitude < data_area.area.max.longitude - data_area.area.min.longitude) {
			// Sort by latitude.
			rs::sort(data_area.nodes, Coords::less_latitude, coords_from_poi_node);
		}
		else {
			// Sort by longitude. But make sure that we get the correct order inside the Area even if we have an E -> W wrap around.
			auto const less_longitude_starting_at{
				[longitude = data_area.area.min.longitude](Coords const& lhs, Coords const& rhs) noexcept { return lhs.longitude - longitude < rhs.longitude - longitude; }
			};
			rs::sort(data_area.nodes, less_longitude_starting_at, coords_from_poi_node);
		}

		// How many subareas do we need for the target number of pois per area? If it is greater than the maximum number that we want, then use the default number.
		std::size_t subareas{ (size(data_area.nodes) + target_pois_per_area - 1) / target_pois_per_area };
		if (subareas > max_subareas) {
			subareas = default_subareas;
		}
		// Vector for the areas, reserve enough space.
		std::vector<std::unique_ptr<Node>> areas;
		areas.reserve(subareas);
		// Number of elements per subarea according to the area size and the chosen number of subareas.
		std::size_t const elements_per_subarea{ (size(data_area.nodes) + subareas - 1) / subareas };
		for (std::size_t i{}; i < subareas; ++i) {
			// First and last index into the original poi vector.
			auto const index_first{ i * elements_per_subarea };
			auto const index_last{ std::min((i + 1) * elements_per_subarea, size(data_area.nodes)) };
			// DataArea for the new subarea, pre-allocate enough space, then move all pois into the subarea.
			DataArea data_subarea{};
			data_subarea.nodes.reserve(index_last - index_first);
			std::move(begin(data_area.nodes) + index_first, begin(data_area.nodes) + index_last, std::back_inserter(data_subarea.nodes));
			// Need to (re)calculate the area and flags of the subarea. No need to recalculate our area and flags as they don't change.
			data_subarea.recalculate_area();
			data_subarea.recalculate_flags();
			// Create the new subarea node and append it
			areas.push_back(std::make_unique<Node>(std::move(data_subarea)));
			// Split the subarea
			split(*areas.back());
		}
		// Overwrite our nodes array with the new subareas array
		data_area.nodes = std::move(areas);
	}

}	// namespace gpilib
