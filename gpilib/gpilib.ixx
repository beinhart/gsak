module;

#include <bit>
#include <chrono>
#include <cstdint>
#include <iostream>
#include <memory>
#include <optional>
#include <string>
#include <tuple>
#include <variant>
#include <vector>

#include <gsl/narrow>

export module gpilib;

static_assert(std::endian::native == std::endian::little, "The code for loading and writing a gpi file currently only works on little endian platforms.");

export namespace gpilib {
	std::string to_cp1252(std::wstring const& str);

	struct Node;

	struct LatLon {
		constexpr LatLon() noexcept = default;
		explicit constexpr LatLon(std::int32_t value_) noexcept
			: value{ value_ }
		{}
		explicit constexpr LatLon(double degrees) noexcept
			: value{ static_cast<std::int32_t>(degrees / 180 * (1U << 31)) }
		{}

		std::int32_t value{};

		[[nodiscard]] auto operator<=>(LatLon const&) const noexcept = default;

		[[nodiscard]] std::uint32_t operator-(LatLon other) const noexcept { return gsl::narrow_cast<std::uint32_t>(value) - gsl::narrow_cast<std::uint32_t>(other.value); }

		[[nodiscard]] constexpr double degrees() const { return static_cast<double>(value) / (1U << 31) * 180; }

		[[nodiscard]] std::pair<int, unsigned> degrees_milliminutes() const;
	};

	LatLon latitude(std::wstring const& str);
	LatLon longitude(std::wstring const& str);

	struct Coords {
		Coords() noexcept = default;
		Coords(LatLon latitude_, LatLon longitude_) noexcept
			: latitude{ latitude_ }
			, longitude{ longitude_ }
		{}

		LatLon latitude{};
		LatLon longitude{};

		[[nodiscard]] auto operator<=>(Coords const&) const noexcept = default;

		friend std::ostream& operator<<(std::ostream& os, Coords const& coords);

		static [[nodiscard]] bool less_latitude(Coords const& lhs, Coords const& rhs) noexcept { return lhs.latitude < rhs.latitude; }
		static [[nodiscard]] bool less_longitude(Coords const& lhs, Coords const& rhs) noexcept { return lhs.longitude < rhs.longitude; }
	};

	struct Area {
		Area() noexcept = default;
		Area(Coords const& min_, Coords const& max_) noexcept;

		Coords min;
		Coords max;

		[[nodiscard]] auto operator<=>(Area const&) const noexcept = default;

		friend std::ostream& operator<<(std::ostream& os, Area const& area);

		[[nodiscard]] bool covers(Area const& other) noexcept;
		[[nodiscard]] bool covers(Coords const& coords) noexcept;

		[[nodiscard]] double surface() const noexcept;
	};

	struct String {
		String() = default;
		explicit String(std::string str_) noexcept
			: str{ std::move(str_) }
		{}

		std::string str;

		bool empty() const noexcept { return str.empty(); }

		std::size_t len() const noexcept;

		std::istream& read(std::istream& is, std::size_t maxlen);
		std::ostream& write(std::ostream& os) const;
		friend std::ostream& operator<<(std::ostream& os, String const& str);
	};

	struct LocalizedString {
		LocalizedString() = default;
		explicit LocalizedString(std::string str_, std::array<char, 2> language_ = { { 'E', 'N' } })
			: language{ language_ }
			, str{ std::move(str_) }
		{}
		explicit LocalizedString(std::wstring const& str_, std::array<char, 2> language_ = { { 'E', 'N' } })
			: language{ language_ }
			, str{ to_cp1252(str_) }
		{}

		std::array<char, 2> language{ { 'E', 'N' } };
		String str;

		std::string_view language_as_sv() const noexcept { return std::string_view{ begin(language), end(language) }; }
		bool empty() const noexcept { return str.empty(); }

		std::size_t len() const noexcept;

		std::istream& read(std::istream& is, std::size_t maxlen);
		std::ostream& write(std::ostream& os) const;
		friend std::ostream& operator<<(std::ostream& os, LocalizedString const& str);
	};

	struct StatisticsAreas {
		StatisticsAreas() noexcept = default;
		StatisticsAreas(std::size_t count_areas_direct_, std::size_t count_areas_total_, std::size_t count_pois_direct_, std::size_t count_pois_total_, std::size_t max_depth_, Area min_area_ = {}) noexcept
			: count_areas_direct{ count_areas_direct_ }
			, count_areas_total{ count_areas_total_ }
			, count_pois_direct{ count_pois_direct_ }
			, count_pois_total{ count_pois_total_ }
			, max_depth{ max_depth_ }
			, min_area{ min_area_ }
		{}

		std::size_t count_areas_direct{};
		std::size_t count_areas_total{};
		std::size_t count_pois_direct{};
		std::size_t count_pois_total{};
		std::size_t max_depth{};
		std::optional<double> percent_surface{};   // How many percent of the upper Area does this Area cover?
		Area min_area{};

		static StatisticsAreas sum_counters(StatisticsAreas const& lhs, StatisticsAreas const& rhs) noexcept;
	};

	// First entry in file.
	struct DataStart {
		DataStart() noexcept = default;
		DataStart(std::chrono::system_clock::time_point time_, std::string name_, std::unique_ptr<Node> node_ = {});

		std::chrono::system_clock::time_point time;
		String name;
		// Optional StartExt node. I have seen this only in Garmin (downloaded) POI files, not for POILoader or GSAK.
		std::unique_ptr<Node> node;

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept;

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
	};

	// Second entry in file.
	struct DataCodepage {
		constexpr DataCodepage() noexcept = default;
		explicit DataCodepage(std::uint16_t codepage_, std::unique_ptr<Node> node_ = {}) noexcept;

		std::uint16_t codepage{};
		// Optional License node. I have seen this only in Garmin (downloaded) POI files, not for POILoader or GSAK.
		std::unique_ptr<Node> node;

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept;

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
	};

	// Entries under Area (either Area with Areas or Area with Pois).
	struct DataPoi {
		DataPoi() noexcept = default;
		DataPoi(LocalizedString name_, Coords coords_) noexcept
			: coords{ coords_ }
			, flags{}
			, name{ name_ }
		{}

		Coords coords{};
		static constexpr std::uint16_t Flag_has_proximity{ 0x0100 };   // Is there any DataProximityAlarm?
		static constexpr std::uint16_t Flag_Garmin{ 0x0200 };		   // Seen in Garmin (downloaded) POI files
		std::uint16_t flags{};
		LocalizedString name;
		// These can be ProximityAlarm, IndexBmp, IndexSubgroup, Cmt, Address, Contact and Desc.
		// IndexSubgroup is present in Garmin (downloaded) POI files. In POILoader files it is present if the group is for a folder
		// and not for a index.
		std::vector<std::unique_ptr<Node>> nodes;

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept;

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;

		bool has_proximity() const noexcept;
	};

	// Optional entry under Poi.
	struct DataProximityAlarm {
		std::uint16_t distance{};	// m
		std::uint16_t speed{};		// m/100 s
		char type{};
		char subtype{};

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept { return {}; }

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
	};

	// Optional either entry under Poi or entries under Subgroup.
	struct DataIndexBmp {
		constexpr DataIndexBmp() noexcept = default;
		explicit constexpr DataIndexBmp(std::uint16_t index_) noexcept
			: index{ index_ }
		{}

		std::uint16_t index{};

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept { return {}; }

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
	};

	// Optional entries under Group metadata.
	struct DataBmp {
		DataBmp() noexcept = default;

		struct Color {
			Color() noexcept = default;
			Color(unsigned char r_, unsigned char g_, unsigned char b_) noexcept
				: r{ r_ }
				, g{ g_ }
				, b{ b_ }
			{}

			unsigned char r{};
			unsigned char g{};
			unsigned char b{};
			unsigned char zero{};
		};

		std::uint16_t index{};
		std::uint16_t width{};
		std::uint16_t height{};
		std::uint16_t bitcount{};
		std::uint32_t unknown{};	// 0 for Garmin (downloaded) POI files, 1 for GSAK and POILoader files.
		std::vector<char> pixels;	// top down (top line first, bottom line last)
		std::vector<Color> palette;
		std::size_t paddingBytes{};	  // Seen only once in a POILoader file.

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept { return {}; }

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
	};

	// Optional entry under Poi.
	struct DataIndexSubgroup {
		constexpr DataIndexSubgroup() noexcept = default;
		explicit constexpr DataIndexSubgroup(std::uint16_t index_) noexcept
			: index{ index_ }
		{}

		std::uint16_t index{};

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept { return {}; }

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
	};

	// Optional entries under Group metadata.
	struct DataSubgroup {
		DataSubgroup() noexcept = default;
		DataSubgroup(std::uint16_t index_, LocalizedString name_, std::unique_ptr<Node> node_ = {});

		std::uint16_t index{};	 // Index of the index, as referenced by DataIndexSubgroup
		LocalizedString name;	 // Name used for display under Extras/"User POIs"/x/... or Extras/"User POIs"/x/...
		// Optional IndexBmp
		std::unique_ptr<Node> node;

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept;

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
	};

	// Entries under Group. Optional entries under Area (either Area with Areas or Area with Pois)
	class DataArea {
	public:
		Area area{};
		static constexpr std::uint16_t Flag_has_proximity{ 0x0100 };   // Is there any DataProximityAlarm?
		static constexpr std::uint16_t Flag_Garmin{ 0x0200 };		   // Seen in Garmin (downloaded) POI files
		std::uint16_t flags{};

		// Either all are Area or all are Poi.
		// Different logic seen for managing areas:
		// Garmin (downloaded) POI files have at most 128 entries per area and the areas can overlap.
		// POILoader creates a top level area containing all pois and with minLongitude <= maxLongitude, then builds a
		// quad tree until all area nodes contain less than x elements (maximum elements seen in one area: 510,
		// minimum elements seen where area was split: 516).
		std::vector<std::unique_ptr<Node>> nodes;

		std::optional<StatisticsAreas> statistics;

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept;

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;

		void update_statistics();

		void recalculate_area();
		void recalculate_flags();

		bool has_proximity() const;

	private:
		void recalculate_area_impl();
	};

	// Third entry in file.
	struct DataGroup {
		DataGroup() = default;
		explicit DataGroup(LocalizedString name_)
			: name{ name_ }
		{}

		LocalizedString name;
		// Node of type Area, containing the Pois
		std::unique_ptr<Node> area;
		// The meta data for this group (Bmp, Subgroup or Mp3).
		// POILoader searches for *.csv and *.gpx. For each index x it searches for x.bmp, x.mp3 and x.wav, so Wav should also be visible here,
		// but I was not able to create a gpi index with Wav embedded.
		// Each data can be given up to once.
		// If the group is for a folder and not for a index, then for each index the data might be available.
		std::vector<std::unique_ptr<Node>> metadata;

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept;

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
	};

	// Optional entry under Poi.
	struct DataCmt {
		DataCmt() = default;
		explicit DataCmt(LocalizedString cmtdesc_)
			: cmtdesc{ cmtdesc_ }
		{}

		LocalizedString cmtdesc;

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept { return {}; }

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
	};

	// Optional entry under Poi.
	struct DataAddress {
		LocalizedString city;
		LocalizedString country;
		LocalizedString state;
		String postcode;
		LocalizedString street;
		String housenr;

		// Empty for Garmin (downloaded) POI files, 2 for POILoader files.
		std::optional<std::size_t> _len1;

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept { return _len1; }

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
	};

	// Optional entry under Poi.
	struct DataContact {
		String phone;
		String mail;
		String web;

		// Empty for Garmin (downloaded) POI files and POILoader files, but 2 for GSAK files.
		std::optional<std::size_t> _len1;

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept { return _len1; }

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
	};

	// Optional entry under Poi.
	struct DataDesc {
		bool garmin;
		LocalizedString desc;

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept { return {}; }

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
	};

	// Optional entry under Start.
	struct DataStartExt {
		std::array<std::uint8_t, 5> data{};

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept { return {}; }

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
	};

	// Optional entry under Codepage.
	struct DataLicense {
		DataLicense() noexcept = default;
		DataLicense(LocalizedString name_, LocalizedString license_)
			: name{ name_ }
			, license{ license_ }
		{}

		// This is the name to be displayed under Extras/. Only this entry is needed for display under Extras/ instead of Extras/"User POIs".
		LocalizedString name{};
		LocalizedString license{};

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept { return {}; }

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
	};

	// Optional entries under Group metadata.
	struct DataMp3 {
		char index;
		std::array<char, 2> language{};
		std::vector<char> mp3;

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept;

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
	};

	// Last entry in file.
	struct DataEof {
		std::size_t len() const noexcept { return 0; }
		std::optional<std::size_t> len1() const noexcept { return {}; }

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const noexcept { return os; }
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
	};

	// Any data that is (yet) unknown to us.
	struct DataOther {
		std::vector<char> data;
		std::optional<std::size_t> _len1;

		std::size_t len() const noexcept;
		std::optional<std::size_t> len1() const noexcept { return _len1; }

		std::istream& read(std::istream& is, std::size_t maxlen, std::optional<std::size_t> maxlen1);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
	};

	enum class Tag : uint16_t {
		Start		   = 0,
		Codepage	   = 1,
		Poi			   = 2,
		ProximityAlarm = 3,
		IndexBmp	   = 4,
		Bmp			   = 5,
		IndexSubgroup  = 6,
		Subgroup	   = 7,
		Area		   = 8,
		Group		   = 9,
		Cmt			   = 10,
		Address		   = 11,
		Contact		   = 12,

		Desc	 = 14,
		StartExt = 15,

		License = 17,
		Mp3		= 18,

		Eof = 0xffff,
	};

	struct Node {
		Node() noexcept = default;
		explicit Node(Tag tag_) noexcept
			: tag{ tag_ }
		{}
		explicit Node(DataStart data_) noexcept
			: tag{ Tag::Start }
			, data{ std::move(data_) }
		{}
		explicit Node(DataCodepage data_) noexcept
			: tag{ Tag::Codepage }
			, data{ std::move(data_) }
		{}
		explicit Node(DataPoi data_) noexcept
			: tag{ Tag::Poi }
			, data{ std::move(data_) }
		{}
		explicit Node(DataProximityAlarm data_) noexcept
			: tag{ Tag::ProximityAlarm }
			, data{ std::move(data_) }
		{}
		explicit Node(DataIndexBmp data_) noexcept
			: tag{ Tag::IndexBmp }
			, data{ std::move(data_) }
		{}
		explicit Node(DataBmp data_) noexcept
			: tag{ Tag::Bmp }
			, data{ std::move(data_) }
		{}
		explicit Node(DataIndexSubgroup data_) noexcept
			: tag{ Tag::IndexSubgroup }
			, data{ std::move(data_) }
		{}
		explicit Node(DataSubgroup data_) noexcept
			: tag{ Tag::Subgroup }
			, data{ std::move(data_) }
		{}
		explicit Node(DataArea data_) noexcept
			: tag{ Tag::Area }
			, data{ std::move(data_) }
		{}
		explicit Node(DataGroup data_) noexcept
			: tag{ Tag::Group }
			, data{ std::move(data_) }
		{}
		explicit Node(DataCmt data_) noexcept
			: tag{ Tag::Cmt }
			, data{ std::move(data_) }
		{}
		explicit Node(DataAddress data_) noexcept
			: tag{ Tag::Address }
			, data{ std::move(data_) }
		{}
		explicit Node(DataContact data_) noexcept
			: tag{ Tag::Contact }
			, data{ std::move(data_) }
		{}
		explicit Node(DataDesc data_) noexcept
			: tag{ Tag::Desc }
			, data{ std::move(data_) }
		{}
		explicit Node(DataStartExt data_) noexcept
			: tag{ Tag::StartExt }
			, data{ std::move(data_) }
		{}
		explicit Node(DataLicense data_) noexcept
			: tag{ Tag::License }
			, data{ std::move(data_) }
		{}
		explicit Node(DataMp3 data_) noexcept
			: tag{ Tag::Mp3 }
			, data{ std::move(data_) }
		{}
		explicit Node(DataEof data_) noexcept
			: tag{ Tag::Eof }
			, data{ std::move(data_) }
		{}

		Tag tag{};
		std::variant<
			DataStart,
			DataCodepage,
			DataPoi,
			DataProximityAlarm,
			DataIndexBmp,
			DataBmp,
			DataIndexSubgroup,
			DataSubgroup,
			DataArea,
			DataGroup,
			DataCmt,
			DataAddress,
			DataContact,
			DataDesc,
			DataStartExt,
			DataLicense,
			DataMp3,
			DataEof,
			DataOther>
			data{};

		std::size_t hdrLen() const noexcept;

		static std::size_t hdrLen(bool withLen1) noexcept;

		std::size_t len() const noexcept;

		std::istream& read(std::istream& is, std::size_t maxlen);
		std::ostream& write(std::ostream& os) const;
		std::ostream& print(std::ostream& os, std::size_t indentation) const;
		friend std::ostream& operator<<(std::ostream& os, Node const& node) { return node.print(os, 0); }
	};

	struct Gpi {
		// First node is Start, second is Codepage, then Groups follow and last is Eof.
		std::vector<Node> nodes;

		std::istream& read(std::istream& is);
		std::ostream& write(std::ostream& os) const;
		friend std::ostream& operator<<(std::ostream& os, Gpi const& gpi);
	};

	// Split an Area that contains Pois into sub areas.
	struct AreaSplittingNTree {
		std::size_t max_pois_per_area{ 128 };
		std::size_t target_pois_per_area{ 128 };
		std::size_t default_subareas{ 3 };
		std::size_t max_subareas{ 6 };

		/* Splits the given area into sub areas.
		 * If an area has less than max_pois_per_area nodes, then it is not split.
		 * Otherwise it is split by latitude or by longitude, whatever the bounding rectangle biggest side is.
		 * Try to split into as many sub areas as are necessary to have target_pois_per_area nodes per area.
		 * If the number of sub areas would exceed max_subareas, then split into default_subareas sub areas.
		 * Apply the function recursively on any sub area.
		 * The bounding areas for the sub areas are set to the minimum bounding rectangle.
		 */
		void split(Node& node_area);
	};

}	// namespace gpilib
